package ru.alexp.tools.myrepo.util;

import org.apache.commons.io.FileUtils;
import ru.alexp.tools.myrepo.Const;

import java.io.File;

public class AppData {

    private static File root;

    public static File getAppDir() {
        if (root == null) {
            String target;

            target = System.getenv("appdata");
            if (target == null) target = System.getenv("user.home");
            if (target == null) target = ".";
            root = FileUtils.getFile(target, "AlexSoft", Const.appName);
            root.mkdirs();
        }
        return root;
    }

    public static File getFile(String... file) {
        File target = FileUtils.getFile(getAppDir(), file);
        target.getParentFile().mkdirs();
        return target;
    }

    public static File getDir(String... dir) {
        File file = getFile(dir);
        file.mkdirs();
        return file;
    }
}
