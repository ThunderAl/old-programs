package ru.alexp.tools.myrepo;

import org.apache.commons.codec.DecoderException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class RFile {

    private static final Logger log = LogManager.getLogger();
    public static final RFile empty = new RFile(null, null);

    private boolean isLocal = false;
    private File local;
    private md5 md5;
    private int size = 0;

    public RFile() {
        this(null, null);
    }

    public RFile(File local) {
        this(null, local);
    }

    public RFile(md5 md5) {
        this(md5, null);
    }

    public RFile(md5 md5, File local) {
        this.md5 = md5;
        this.local = local;
        if (this.local != null && this.local.exists()) isLocal = true;
        else if (this.md5 == null) this.md5 = md5.empty;
    }

    public static RFile fromMd5(String md5) {
        try {
            return new RFile(new md5(md5), null);
        } catch (DecoderException e) {
            return empty;
        }
    }

    public static RFile fromFile(File f) {
        return new RFile(f);
    }

    public static RFile fromFile(String s) {
        return fromFile(new File(s));
    }

    public md5 getMd5() {
        return md5 == null ? (md5 = md5.fromFile(local)) : md5;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public File getLocalFile() {
        return local;
    }

    public void setLocation(File local) {
        this.local = local;
    }

    public void setHash(String md5) {
        try {
            setHash(new md5(md5));
        } catch (DecoderException e) {
            log.error(e);
        }
    }

    public void setHash(md5 md5) {
        this.md5 = md5;
    }

    public boolean equalsByMd5(RFile file) {
        return this.getMd5().equals(file.getMd5());
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
