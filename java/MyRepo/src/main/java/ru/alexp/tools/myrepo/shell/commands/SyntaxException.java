package ru.alexp.tools.myrepo.shell.commands;

public class SyntaxException extends Exception {
    public SyntaxException() {
        super();
    }

    public SyntaxException(String message) {
        super(message);
    }
}
