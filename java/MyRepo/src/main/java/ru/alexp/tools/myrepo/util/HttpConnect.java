package ru.alexp.tools.myrepo.util;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.alexp.tools.myrepo.Const;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

public class HttpConnect {
    private static final Logger log = LogManager.getLogger(HttpConnect.class);
    private static final Logger llog = LogManager.getLogger(Response.class);

    private final String host;
    private String subdomain;
    private String defaultPage = "";

    public HttpConnect(String host) {
        this.host = host;
    }

    private static String encodeParams(String... args) {
        StringBuilder sb = new StringBuilder();
        try {
            for (String arg : args) {
                if (arg == null) {
                    sb.append("null");
                } else if (arg.matches("[\\?&][\\w\\d\\.\\-]+=")) {
                    sb.append(arg);
                } else {
                    sb.append(URLEncoder.encode(arg, "UTF-8"));
                }
            }
        } catch (UnsupportedEncodingException e) {
            log.warn(e);
        }
        return sb.toString();
    }

    private HttpURLConnection getConnection(String page) {
        try {
            URL target = new URL(
                    "http://" +
                            (subdomain == null || subdomain.isEmpty() ? "" : subdomain + ".") +
                            this.host + ((page == null ? defaultPage : page)
                            .startsWith("/") ? "" : "/") +
                            (page == null ? defaultPage : page)
            );
            return ((HttpURLConnection) target.openConnection());
        } catch (IOException e) {
            log.warn(e);
            return null;
        }
    }

    public HttpURLConnection getHttpURLConnection(String page, String method, String... args) throws Exception {

        HttpURLConnection conn;

        page = page == null ? defaultPage : page;

        if (method.equalsIgnoreCase("get")) {
            page += encodeParams(args);
            args = null;
        }

        if ((conn = getConnection(page)) == null) return null;

        String data = null;

        if (args != null) {
            data = encodeParams(args);
            conn.setRequestMethod(method);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Content-Length", Integer.toString(data.getBytes().length));
        }

        conn.setRequestProperty("User-Agent", "MyRepo");
        conn.setRequestProperty("Repo-version", Const.version);
        conn.setUseCaches(false);
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setConnectTimeout(5000);
        conn.setReadTimeout(10000);
        conn.connect();

        if (data != null) {
            try (BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()))) {
                wr.write(data);
                wr.flush();
            }
        }

        return conn;
    }

    public Response query(String page, String method, String... args) {
        HttpURLConnection conn = null;
        try {
            conn = getHttpURLConnection(page, method, args);
            return new Response(IOUtils.toByteArray(conn.getInputStream()), conn.getResponseCode(), conn.getHeaderFields());
        } catch (Exception e) {
            return new Response(e.getMessage());
        } finally {
            if (conn != null)
                conn.disconnect();
        }
    }

    public Response get(String page, String... args) {
        return query(page, "GET", args);
    }

    public Response post(String page, String... args) {
        return query(page, "POST", args);
    }

    public HttpConnect setSubdomain(String subdomain) {
        this.subdomain = subdomain;
        return this;
    }

    public HttpConnect setDefaultPage(String page) {
        this.defaultPage = page;
        return this;
    }

    public class Response {
        private final byte[] data;
        private final boolean isConnected;
        private final String reason;
        private final int responseCode;
        private final Map<String, List<String>> headers;

        public Response(byte[] data, int responseCode, Map<String, List<String>> headers) {
            this.data = data;
            this.isConnected = true;
            this.reason = null;
            this.responseCode = responseCode;
            this.headers = headers;
        }

        public Response(String reason) {
            this.data = null;
            this.isConnected = false;
            this.reason = reason;
            this.responseCode = 0;
            this.headers = null;
        }

        @Override
        public String toString() {
            try {
                return new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                llog.warn(e);
                return "";
            }
        }

        public byte[] getData() {
            return data;
        }

        public boolean isConnected() {
            return isConnected;
        }

        public String getReason() {
            return reason;
        }

        public int getResponseCode() {
            return responseCode;
        }

        public Map<String, List<String>> getHeaders() {
            return headers;
        }

        public String getHost() {
            if (subdomain == null) return host;
            else return subdomain + "." + host;
        }
    }
}
