package ru.alexp.tools.myrepo.shell.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CommandManager {

    private final ArrayList<Command> commands;
    private final Map<String, Command> names;

    public CommandManager() {
        commands = new ArrayList<>();
        names = new HashMap<>();
    }

    private static String parseAliases(String... aliases) {
        if (aliases == null || aliases.length == 0) return null;
        final StringBuilder sb = new StringBuilder();
        sb.append("Aliases: [");
        for (int i = 0; i < aliases.length; i++) {
            sb.append(aliases[i]);
            if (i < aliases.length - 1) sb.append(", ");
        }
        sb.append(']');
        return sb.toString();
    }

    public void addCommand(Command c) {
        c.setCommandManager(this);
        commands.add(c);
        names.put(c.getName(), c);
        String[] aliases = c.getAliases();
        if (aliases != null && aliases.length > 0)
            for (String alias : aliases)
                names.put(alias, c);
    }

    public void invokeCommand(String name, String... args) {
        Command c = names.get(name);
        if (c == null) {
            System.err.println("Command \"" + name + "\" not found!");
            return;
        }
        try {
            c.onCommand(args);
        } catch (SyntaxException e) {
            String message = e.getMessage();
            if (message == null)
                System.err.println("Syntax error.");
            else
                System.err.println(message);
        }
    }

    public String[] getHelp() {
        String[] help = new String[commands.size() + 1];
        help[0] = "Commands:";
        for (int i = 0; i < commands.size(); i++) {
            Command c = commands.get(i);
            String name = c.getName();
            String descr = c.getDescr();
            String syntax = c.getSyntax();
            String aliases = parseAliases(c.getAliases());
            help[i + 1] = name + (syntax == null ? "" : " " + syntax) + (descr == null ? "" : " - " + descr) + (aliases == null ? "" : " " + aliases);
        }
        return help;
    }
}
