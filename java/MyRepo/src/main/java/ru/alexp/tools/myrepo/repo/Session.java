package ru.alexp.tools.myrepo.repo;

import ru.alexp.tools.myrepo.util.HttpConnect;

public class Session {

    private static Session ses = new Session(User.anonimus);

    private final User user;

    public Session(User u) {
        user = u;
    }

    public static Session getCurSession() {
        return ses;
    }

    public User getUser() {
        return user;
    }
}
