package ru.alexp.tools.myrepo.repo.http;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.repo.*;
import ru.alexp.tools.myrepo.repo.downloaders.Downloader;
import ru.alexp.tools.myrepo.util.HttpConnect;
import ru.alexp.tools.myrepo.util.JSONUtil;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

public class HttpRepo extends Repo {

    public static final List<String> subdomains = Arrays.asList("", "repo", "myrepo", "files", "mr");
    public static final List<String> pages = Arrays.asList("", "repo.php", "repo", "myrepo", "files", "mr", "p/repo", "p/myrepo", "p/files", "p/mr");

    private final HttpConnect conn;

    Logger log = LogManager.getLogger(HttpRepo.class);

    public HttpRepo(URI location) throws RepoException {
        super(location, Type.HTTP);
        conn = findRepo();
    }

    @Override
    public void loadCollectionList() throws RepoException {
        try {
            HttpConnect.Response response = conn.get(null, "?action=", "getCollections");
            JSONObject json = new JSONObject(response.toString());
            if (json.getBoolean("success")) {
                JSONArray array = json.getJSONArray("collections");
                try {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject col = array.getJSONObject(i);
                        collections.add(new Collection(
                                col.optInt("id", i), col.getString("name"), col.optString("descr"),
                                col.optInt("createts", -1), col.optInt("editts", -1),
                                JSONUtil.toStringArrayList(col.getJSONArray("tags")),
                                JSONUtil.parseRFilesArrayList(col.getJSONArray("files"))
                        ));
                    }
                } catch (Exception e) {
                    log.warn(e);
                }
            } else throw new RepoException(json.optString("error"));
        } catch (JSONException e) {
            throw new RepoException(e);
        }
    }

    private HttpConnect findRepo() throws RepoException {
        HttpConnect conn;
        try {
            conn = new HttpConnect((location.getHost() == null ? location.toString() : location.getHost()) +
                    (location.getPort() == -1 ? "" : ":" + location.getPort()));
            if (checkConnect(conn, location.getPath())) return conn.setDefaultPage(location.getPath());
        } catch (Exception e) {
        }
        for (String subdomain : subdomains) {
            conn = new HttpConnect((location.getHost() == null ? location.toString() : location.getHost()) +
                    (location.getPort() == -1 ? "" : ":" + location.getPort()))
                    .setSubdomain(subdomain);
            for (String page : pages) {
                if (checkConnect(conn, page)) return conn.setDefaultPage(page);
            }
        }
        throw new RepoException("repo.notFound");
    }

    private boolean checkConnect(HttpConnect conn, String page) throws RepoException {
        try {
            HttpConnect.Response response = conn.get(page,
                    "?action=", "info", "&version=", Const.version,
                    "&user=", Session.getCurSession().getUser().getLogin(),
                    "&password=", Session.getCurSession().getUser().getPassword(),
                    "&passkeys=", String.join(":", Session.getCurSession().getUser().getPasskey())
            );
            if (!response.isConnected()) return false;
            if (response.getResponseCode() != 200) return false;
            final JSONObject json = new JSONObject(response.toString());
            if (!json.getString("app").equalsIgnoreCase(Const.appName)) return false;
            if (json.getString("status").equalsIgnoreCase("ok")) return true;
            else if (json.getString("status").equalsIgnoreCase("forbidden"))
                throw new ru.alexp.tools.myrepo.repo.SecurityException(json.getString("status"));
            else throw new RepoException(json.getString("status"));
        } catch (Exception e) {
            return false;
        }
    }

    HttpConnect getConn() {
        return conn;
    }

    @Override
    public Downloader getDownloader() throws RepoException {
        return new HttpDownloader(this);
    }
}
