package ru.alexp.tools.myrepo.crypt;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Класс для упрощённой работы кодирующим алгоритмом AES 16bit
 */
public class AES128 {

    public static final int keyLength = 16;
    private static final Logger log = LogManager.getLogger();
    private static final byte fillKeyByte = (byte) 0;

    private Cipher encCipher;
    private Cipher decCipher;
    private SecretKeySpec keySpec;

    /**
     * @param key ключ шифрования/дешифрования. (желательно 16 байт)
     */
    public AES128(byte[] key) {
        setKey(key);
    }

    /**
     * Создание шифровщика с уже созданным ключём
     *
     * @param key ключ шифрования/дешифрования.
     */
    public AES128(SecretKeySpec key) {
        setKey(key);
    }

    /**
     * Инициализация шифрататора с ключём "null"
     */
    public AES128() {
        setKey("null".getBytes());
    }

    /**
     * Устанавливает новый ключ шифрования.
     * При этом будут обновлены существующие шифровщики.
     *
     * @param key ключ шифрования/дешифрования. (желательно 16 байт)
     */
    public AES128 setKey(byte[] key) {
        if (key.length != keyLength) {
            key = Arrays.copyOf(key, keyLength);
        }
        return setKey(new SecretKeySpec(key, "AES"));
    }

    /**
     * Устанавливает новый ключ шифрования.
     * При этом будут обновлены существующие шифровщики.
     *
     * @param key ключ шифрования/дешифрования. (желательно 16 байт)
     */
    public AES128 setKey(SecretKeySpec key) {
        keySpec = key;
        refreshDecCipher();
        refreshEncCipher();

        return this;
    }

    SecretKeySpec getKeySpec() {
        return keySpec;
    }

    /**
     * @return шифрующий модуль
     */
    public Cipher getEncCipher() {
        Cipher c = encCipher;
        if (encCipher == null) {
            try {
                c = (encCipher = Cipher.getInstance("AES"));
                try {
                    encCipher.init(Cipher.ENCRYPT_MODE, keySpec);
                } catch (InvalidKeyException e) {
                    log.error(e);
                }
            } catch (NoSuchPaddingException | NoSuchAlgorithmException e) {
                log.error(e);
            }
        }
        return c;
    }

    /**
     * @return дешифрующий модуль
     */
    public Cipher getDecCipher() {
        Cipher c = decCipher;
        if (decCipher == null) {
            try {
                c = (decCipher = Cipher.getInstance("AES"));
                try {
                    decCipher.init(Cipher.DECRYPT_MODE, keySpec);
                } catch (InvalidKeyException e) {
                    log.error(e);
                }
            } catch (NoSuchPaddingException | NoSuchAlgorithmException e) {
                log.error(e);
            }
        }
        return c;
    }

    public void refreshEncCipher() {
        try {
            if (keySpec != null && encCipher != null)
                encCipher.init(Cipher.ENCRYPT_MODE, keySpec);
        } catch (InvalidKeyException e) {
            log.error(e);
        }
    }

    public void refreshDecCipher() {
        try {
            if (keySpec != null && decCipher != null)
                decCipher.init(Cipher.ENCRYPT_MODE, keySpec);
        } catch (InvalidKeyException e) {
            log.error(e);
        }
    }

    public byte[] encryptData(byte[] data) {
        try {
            Cipher c = getEncCipher();
            return c.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            log.error(e);
            return new byte[0];
        }
    }

    public String encryptDataToHash(byte[] data) {
        return Base64.encodeBase64String(encryptData(data));
    }

    public byte[] decryptData(byte[] data) {
        try {
            Cipher c = getDecCipher();
            return c.doFinal(data);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            log.error(e);
            return new byte[0];
        }
    }

    public String decryptDataToString(byte[] data) {
        try {
            return new String(decryptData(data), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error(e);
            return "";
        }
    }

    public String decryptHashToString(String hash) {
        return decryptDataToString(Base64.decodeBase64(hash));
    }
}
