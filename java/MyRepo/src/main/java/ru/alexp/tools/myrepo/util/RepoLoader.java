package ru.alexp.tools.myrepo.util;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import ru.alexp.tools.myrepo.repo.Repo;

import java.io.File;
import java.util.ArrayList;

public class RepoLoader {
    private static final Logger log = LogManager.getLogger(RepoLoader.class);
    private final JSONObject json;
    private final ArrayList<Repo> repos;

    public RepoLoader() {
        final File f = AppData.getFile("myrepo.json");
        if (!f.exists()) {
            log.trace("RepoLoader file not found");
            json = new JSONObject();
        } else {
            JSONObject jt = new JSONObject(); // временная переменная для блока try
            try {
                log.trace("Loading repos from %s", f.getAbsolutePath());
                jt = new JSONObject(IOUtils.toString(f.toURI(), "UTF-8"));
            } catch (Exception e) {
                log.warn("Can't read %s, %s", f.getName(), e.getMessage());
            } finally {
                json = jt;
            }
        }

        repos = new ArrayList<>();
        JSONArray ja = json.getJSONArray("repo");
        for (int i = 0; i < ja.length(); i++) {
            Repo r = Repo.parseRepo(ja.getJSONObject(i));
            if (r != null) repos.add(r);
        }

        log.info("Loaded %d repo", repos.size());
    }
}
