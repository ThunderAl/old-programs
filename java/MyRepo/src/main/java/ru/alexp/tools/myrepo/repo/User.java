package ru.alexp.tools.myrepo.repo;

import ru.alexp.tools.myrepo.md5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringJoiner;

public class User implements Serializable {

    public final static User anonimus;

    static {
        anonimus = new User("anonimus", new md5(0).toString(), new ArrayList<>());
    }

    private final String login;
    private final String password;
    private final ArrayList<String> passkey;

    public User(String login, String password, ArrayList<String> passkey) {
        this.login = login;
        this.password = password;
        this.passkey = passkey;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public ArrayList<String> getPasskey(String domain) {
        ArrayList<String> arr = new ArrayList<>();



        return passkey;
    }
}
