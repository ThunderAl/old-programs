package ru.alexp.tools.myrepo;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class md5 {
    public static final md5 empty = new md5();

    private final byte[] md5 = new byte[16];
    private String hash;

    public md5(String hash) throws DecoderException {
        this(Hex.decodeHex(hash.toCharArray()));
        this.hash = hash;
    }

    public md5(byte... bytes) {
        System.arraycopy(bytes, 0, md5, 0, md5.length);
    }

    public md5(int... bytes) {
        for (int i = 0; i < md5.length; i++) md5[i] = (byte) bytes[i];
    }

    public md5() {
        Arrays.fill(md5, (byte) 0);
    }

    public static md5 fromFile(File file) {
        try (InputStream is = new FileInputStream(file)) {
            MessageDigest digest = MessageDigest.getInstance("MD5");

            byte[] buffer = new byte[1024];
            int read;

            while ((read = is.read(buffer)) > 0)
                digest.update(buffer, 0, read);

            return new md5(digest.digest());

        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return empty;
        }
    }

    public byte[] getBytes() {
        return md5;
    }

    @Override
    public String toString() {
        return hash == null ? (hash = Hex.encodeHexString(md5)) : hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof md5) {
            byte[] bytes = ((md5) obj).getBytes();
            for (int i = 0; i < bytes.length; i++)
                if (getBytes()[i] != bytes[i])
                    return false;
            return true;
        } else {
            return super.equals(obj);
        }
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (byte b : getBytes()) result += (int) b;
        return result;
    }
}
