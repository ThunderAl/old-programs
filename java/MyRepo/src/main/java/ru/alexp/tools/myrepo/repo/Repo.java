package ru.alexp.tools.myrepo.repo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import ru.alexp.tools.myrepo.RFile;
import ru.alexp.tools.myrepo.repo.downloaders.Downloader;

import java.net.URI;
import java.util.LinkedList;

public abstract class Repo {
    private static Logger log = LogManager.getLogger(Repo.class);

    protected final URI location;
    protected final Type type;
    protected final LinkedList<Collection> collections;
    private String name;

    public Repo(URI location, Type type) {
        this.location = location;
        this.type = type;
        this.collections = new LinkedList<>();
    }

    public static Repo parseRepo(JSONObject repo) {
        try {
            // TODO: 22.08.2016
        } catch (Exception e) {
            log.warn("Can't load repo %s, %s", repo.getString("name"), e.getMessage());
        }
    }

    /**
     * Грузим список коллекций
     */
    public abstract void loadCollectionList() throws RepoException;

    public abstract Downloader getDownloader() throws RepoException;

    public boolean contains(Collection c) {
        return collections.contains(c);
    }

    public boolean conttains(RFile f) {
        for (Collection collection : collections)
            if (collection.contains(f))
                return true;
        return false;
    }

    public boolean isEmpty() {
        return collections.isEmpty();
    }

    public int size() {
        return collections.size();
    }

    public LinkedList<Collection> getCollections() {
        return collections;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /* при создании нового хранилища добавлять его тип сюда */
    public enum Type {
        HTTP
    }
}
