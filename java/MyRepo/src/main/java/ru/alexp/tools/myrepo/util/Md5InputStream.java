package ru.alexp.tools.myrepo.util;

import ru.alexp.tools.myrepo.Const;
import ru.alexp.tools.myrepo.md5;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5InputStream extends OutputStream {

    private final MessageDigest digest;
    private md5 md5;

    public Md5InputStream() throws NoSuchAlgorithmException {
        digest = MessageDigest.getInstance("MD5");
    }

    @Override
    public void write(int b) throws IOException {
        digest.update((byte) b);
    }

    public void write(byte[] b) throws IOException {
        digest.update(b);
    }

    public void write(byte[] b, int off, int len) throws IOException {
        digest.update(b, off, len);
    }

    public md5 getMd5() {
        if (md5 == null) md5 = new md5(digest.digest());
        return md5;
    }

    public Md5InputStream copyFrom(InputStream is) throws IOException {
        byte[] buffer = new byte[Const.bufferSize];
        int read;

        while ((read = is.read(buffer)) > 0)
            write(buffer, 0, read);

        return this;
    }
}
