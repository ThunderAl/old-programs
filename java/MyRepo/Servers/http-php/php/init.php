<?php

// считаем время загрузки сайта
$mtime = microtime();
$mtime = explode(" ", $mtime);
$_SERVER['MICRONE_START'] = $mtime[1] + $mtime[0];

function registerGlobals()
{
    define('mainDir', dirname(__FILE__));
    define('host', $_SERVER['SERVER_NAME']);
}

function commentInput()
{
    foreach ($_GET as $key => $value) {
        $_GET[$key] = mysql_real_escape_string(htmlspecialchars($value));
    }

    foreach ($_POST as $key => $value) {
        $_POST[$key] = mysql_real_escape_string(htmlspecialchars($value));
    }
}

function loadLoggerSettings()
{
    if (config::getConfig('main', 'isDebug')) {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
    } else {
        ini_set('display_errors', 0);
    }
}

?>