package ru.alexp.fx.tools.gui;

import java.util.ArrayList;
import java.util.Arrays;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import ru.alexp.fx.tools.CommandHandler;
import ru.alexp.fx.tools.swing.WrapperFrame;

/**
 *
 * @author Александр
 */
public class InputPane extends BorderPane {

    private final CommandHandler commandHandler = new CommandHandler();
    
    private final Label consolePreffix;
    private final TextField inputField;
    
    public InputPane() {
        
        consolePreffix = new Label("#");
        consolePreffix.getStyleClass().add("consolePreffixLabel");
        consolePreffix.setPrefHeight(WrapperFrame.getInputFrame().getHeight());
        
        inputField = new TextField();
        inputField.getStyleClass().add("consoleInputField");
        inputField.setOnKeyTyped(e -> {
            if (e.getCharacter().equals("\r")) {
                final String[] in = inputField.getText().split(" ");
                final ArrayList<String> args = new ArrayList<>();
                args.addAll(Arrays.asList(in));
                commandHandler.onCommand(args);
                inputField.setText("");
            }
        });
        
        setLeft(consolePreffix);
        setCenter(inputField);
    }
}
