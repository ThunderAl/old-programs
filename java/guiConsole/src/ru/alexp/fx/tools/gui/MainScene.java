package ru.alexp.fx.tools.gui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author Александр
 */
public class MainScene extends Scene {

    private static MainScene istance;
    private static BorderPane layout;

    private final InputPane inputStage;

    private MainScene(Parent p) {
        super(p);

        inputStage = new InputPane();
        layout.setCenter(inputStage);
        
        setFill(Color.TRANSPARENT);
        getStylesheets().add("ru/alexp/fx/tools/resources/console.css");
    }

    private static BorderPane getBorderPane() {
        if (layout == null) {
            layout = new BorderPane();
        }
        return layout;
    }

    public static MainScene getMainScene() {
        if (istance == null) {
            istance = new MainScene(getBorderPane());
        }
        return istance;
    }
}
