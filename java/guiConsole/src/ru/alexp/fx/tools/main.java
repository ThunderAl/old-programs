package ru.alexp.fx.tools;

import ru.alexp.fx.tools.gui.MainScene;
import ru.alexp.fx.tools.swing.WrapperFrame;

/**
 *
 * @author Александр
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final WrapperFrame wrapper = new WrapperFrame(); // делаем свинговский фрейм так как JavaFX не даёт нам возможность сделать свои окна и прозрачныим и "тулловыми"
        wrapper.getFXPanel().setScene(MainScene.getMainScene());
        wrapper.setVisible(true);
    }

}
