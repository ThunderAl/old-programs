package ru.alexp.fx.tools;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Александр
 */
public class CommandHandler {

    private Process proc;
    private BufferedWriter writer;

    public void onCommand(ArrayList<String> params) {
        if (params.size() > 0) {
            if (params.get(0).equalsIgnoreCase("exit")) {
                if (proc != null) {
                    proc.destroy();
                    proc = null;
                }
                System.exit(0);
            } else {
                if (proc != null && writer != null) {
                    try {
                        writer.write(String.join(" ", params));
                        writer.newLine();
                        writer.flush();
                    } catch (IOException ex) {
                        System.out.println(ex.getLocalizedMessage());
                    }
                } else {
                    final ProcessBuilder pb = new ProcessBuilder(params);
                    try {
                        pb.start(); // пока просто запуск
                    } catch (IOException ex) {
                        System.out.println(ex.getLocalizedMessage());
                    }
                }
            }
        }
    }
    
    public Process getProcess() {
        return proc;
    }
    
    public boolean killProcess() {
        try {
            proc.destroy();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
