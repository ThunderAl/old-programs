package ru.alexp.fx.tools.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import javafx.embed.swing.JFXPanel;
import javax.swing.JFrame;

/**
 *
 * @author Александр
 */
public class WrapperFrame extends JFrame {

    private static WrapperFrame instance;

    private JFXPanel FXWrapper;

    public WrapperFrame() {
        final Dimension dim = getToolkit().getScreenSize();
        setSize(500, 80);
        setLocation(dim.width - getWidth() - 50, dim.height - getHeight() - 200);
        setUndecorated(true);
        setBackground(new Color(255, 255, 255, 0));
        setType(Window.Type.UTILITY);
        add(getFXPanel());
    }

    public JFXPanel getFXPanel() {
        if (FXWrapper == null) {
            FXWrapper = new JFXPanel();
        }
        return FXWrapper;
    }

    public static WrapperFrame getInputFrame() {
        if (instance == null) {
            instance = new WrapperFrame();
        }
        return instance;
    }
}
