package ru.alexp.tool.ddnsmanager

import ru.alexp.tool.ddnsmanager.commands.{CHelp, CList, Command}

object Console {

  def main(args: Array[String]) = command("l") run()

  private def command(command: String): Command = command match {
    case "usage" | "help" | "h" => new CHelp
    case "list" | "l" => new CList
    case _ => new CHelp
  }
}