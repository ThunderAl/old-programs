package ru.alexp.tool.ddnsmanager.commands

import ru.alexp.tool.ddnsmanager.DDNSManager

class CList extends Command {
  override def run() = for (item <- DDNSManager().getList) printf(item)
}
