package ru.alexp.tool.ddnsmanager.rapi

case class Record(host: String, rtype: String, priority: Int = -1) {

  private var changed = false

  def isChanged = changed
  def change() = changed = true
}
