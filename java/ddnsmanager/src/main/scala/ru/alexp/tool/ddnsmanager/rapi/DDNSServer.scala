package ru.alexp.tool.ddnsmanager.rapi

trait DDNSServer {

  def name: String
  def records: List[Record]
}
