package ru.alexp.tool.ddnsmanager.commands

class CHelp extends Command {
  override def run(): Unit = {
    println(
      """
        |Usage: ddnsm <command> [args]
        |
        |Commands:
        | updateAll - checks and updates all ddns entries.
      """.stripMargin)
  }
}
