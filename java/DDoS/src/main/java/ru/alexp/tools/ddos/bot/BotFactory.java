package ru.alexp.tools.ddos.bot;

public interface BotFactory {
    Bot getBot();
}
