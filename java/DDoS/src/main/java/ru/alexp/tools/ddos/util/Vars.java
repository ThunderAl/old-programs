package ru.alexp.tools.ddos.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Александр
 */
public class Vars {

    private static final Logger log = LogManager.getLogger(Vars.class);

    private final Map<String, Object> vars;

    public Vars() {
        vars = new HashMap();
    }

    public void put(String key, Object value) {
        vars.put(key, value);
    }

    public void put(Object... args) {
        if (args.length % 2 != 0)
            throw new RuntimeException("Length of parameters must be even!"); // если параметров не чётное кол-во
        for (int i = 0; args.length > i; i++) {
            if (i % 2 != 0) {
                String key = String.valueOf(args[i]);
                vars.put(key, args[i + 1]);
            }
        }
    }

    public Object get(String key, Object def) {
        if (vars.containsKey(key)) {
            return vars.get(key);
        } else {
            return def;
        }
    }

    public Object get(String key) {
        return get(key, "");
    }

    public boolean containsKey(String key) {
        return vars.containsKey(key);
    }

    public boolean containsValue(String value) {
        return vars.containsValue(value);
    }

    public Map<String, Object> getMap() {
        return vars;
    }

    public void clear() {
        vars.clear();
    }
}
