package ru.alexp.tools.ddos.bot;


import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;

public class ProxyManager {

    private static final Logger log = LogManager.getLogger(ProxyManager.class);



    static {
        log.info("Invoked static body");
    }


    public void loadProxies(String file) {
        try {
            try (InputStream is = getClass().getResourceAsStream(file)) {
                InputStreamReader reader = new InputStreamReader(is);
                LineIterator li = IOUtils.lineIterator(reader);
                while (li.hasNext()) {

                }
            }
        } catch (Throwable t) {
            log.error("Error while loading proxies");
            log.throwing(Level.ERROR, t);
        }
    }

    public void loadDefaultProxyList() {
        loadProxies("proies.csv");
    }

}
