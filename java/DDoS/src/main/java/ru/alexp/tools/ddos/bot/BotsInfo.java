package ru.alexp.tools.ddos.bot;

import ru.alexp.tools.ddos.util.Vars;

/**
 *
 * @author Alex_P
 */
public class BotsInfo {
    
    private static int erroredConection = 0;
    private static int successConection = 0;
    
    private static int connected = 0;
    
    public static void ddosState(boolean state) {
        Vars.put("DdosAcived", state);
    }

    public static boolean isStarted() {
        return Boolean.valueOf(Vars.get("DdosAcived", "true").toString());
    }
    
    static void errConection() {
        erroredConection++;
    }
    
    static void successConection() {
        successConection++;
    }
    
    static void setCountConnected(int i) {
        connected = i;
    }
    
    public static int getCountConnectedBots() {
        return connected;
    }
    
    public static int getCountErrors() {
        return erroredConection;
    }
    
    public static int getCountConnect() {
        return successConection;
    }
}
