package ru.alexp.tools.ddos.bot;

import ru.alexp.tools.ddos.main;

/**
 * @author Alex_P
 */
public abstract class Bot {

    private Enum state = states.IDLE;

    public Bot(String ip, short port) {
        main.getVars().put("targetHost", ip);
        main.getVars().put("targetPort", port);
    }

    public Bot() {
    }

    public abstract void connect();

    public abstract void disconnect();

    public abstract void send();

    public void setHost(String host) {
        main.getVars().put("targetHost", host);
    }

    public void setPort(short port) {
        main.getVars().put("targetPort", port);
    }

    public Enum getStatus() {
        return state;
    }

    public enum states {
        IDLE, ERROR, INACTION
    }
}
