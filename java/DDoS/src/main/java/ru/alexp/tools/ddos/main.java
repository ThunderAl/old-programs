package ru.alexp.tools.ddos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.alexp.tools.ddos.bot.BotsManager;
import ru.alexp.tools.ddos.bot.ProxyManager;
import ru.alexp.tools.ddos.util.Vars;

/**
 * @author Alex_P
 */
public class main {

    private static final Logger log = LogManager.getLogger(main.class);
    private static final int VERSION = 1;
    private static Vars vars;
    private static String[] startupArgs;

    public static void main(String[] args) {

        vars = new Vars();

        // loding logger
        setupLogger();

        // startup args
        startupArgs = args;
        if (getStartupArgs().length > 0) {
            for (int i = 0; getStartupArgs().length > i; i++) {
                if (getStartupArgs()[i].startsWith("-")) {
                    String key = getStartupArgs()[i].replaceAll("-", "");
                    Object value;
                    if (getStartupArgs().length > (i + 1) && !getStartupArgs()[(i + 1)].startsWith("-")) {
                        value = getStartupArgs()[(i + 1)];
                    } else {
                        value = true;
                    }
                    getVars().put(key, value);
                }
            }
        }

        ProxyManager pm = new ProxyManager();
        pm.loadDefaultProxyList();
    }

    public static String[] getStartupArgs() {
        return startupArgs;
    }

    public static Vars getVars() {
        return vars;
    }

    private static void setupLogger() {

    }

    public static void exit() {
        BotsManager.stopDos();
        System.exit(0);
    }

}
