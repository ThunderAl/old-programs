package ru.alexp.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Александр
 */
public class Vars {

    private static final Map<String, Object> config = new HashMap();

    public static void put(String key, Object value) {

        config.put(key, value);
    }

    public static Object get(String key, Object def) {

        if (config.containsKey(key)) {

            return config.get(key);

        } else {

            return def;
        }
    }

    public static Object get(String key) {

        return get(key, "");
    }

    public static boolean containsKey(String key) {

        return config.containsKey(key);
    }

    public static boolean containsValue(String value) {

        return config.containsValue(value);
    }

    public static void clear() {

        config.clear();
    }

    public static boolean isDebug() {

        return Boolean.valueOf(get("debug", false).toString());
    }

    public static Map<String, Object> getMap() {
        return config;
    }

    public static void parseArray(String... args) {
        int index = 0;
        while (index < args.length) {
            if (args[index].startsWith("-")) {
                String key = args[index].replaceAll("^-(.*)", "$1");
                Object value = true;
                if (args.length > index + 1 && !args[index + 1].startsWith("-")) {
                    value = args[index + 1];
                    index++;
                }
                Vars.put(key, value);
            }
            index++;
        }
    }
}
