package ru.alexp.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Alex_P
 */
public class Logger {

    private static Logger logger;
    private final ArrayList<String> log = new ArrayList();
    private static final ArrayList<LogAction> hooks = new ArrayList();

    public static void setupLogger() {

        if (logger != null) {
            debug("Setting up new logger!");
        }

        logger = new Logger();
    }

    public static void addHook(LogAction r) {
        hooks.add(r);
    }

    public static void print(String s, Level level, boolean line) {
        if (logger == null) {
            setupLogger();
        }
        StringBuilder echo = new StringBuilder();

        echo.append(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        echo.append(":");
        echo.append(Calendar.getInstance().get(Calendar.MINUTE));

        if (level == Level.Debug) {
            if (!Vars.isDebug()) {
                return;
            }
        }
        if (level != Level.None) {
            echo.append(" ");
            echo.append("[").append(level.toString().toUpperCase()).append("]");

        }
        echo.append(" ");
        echo.append(s);

        if (line) {
            echo.append("\n");
        }

        if (level == Level.Error) {
            System.err.print(echo.toString());
        } else {
            System.out.print(echo.toString());
        }

        logger.log.add(echo.toString());

        if (!hooks.isEmpty()) {
            hooks.stream().forEach((action) -> {
                action.newLine(echo.toString());
            });
        }

    }

    public static void print(String s, Level level) {
        print(s, level, true);
    }

    public static void none(String s) {
        print(s, Level.None);
    }

    public static void info(String s) {
        print(s, Level.Info);
    }

    public static void debug(String s) {
        print(s, Level.Debug);
    }

    public static void warn(String s) {
        print(s, Level.Warn);
    }

    public static void error(String s) {
        print(s, Level.Error);
    }

    public static void clear() {
        logger.log.clear();
    }

    public static void printStacktrace(Throwable t, Level l, int length) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);
        for (String line : sw.toString().split("\t")) {
            if (length != -1 && length == 0) {
                return;
            }
            Logger.print(line.substring(0, line.length() - 1), l);
            length--;
        }
    }

    public static void printStacktrace(Throwable t) {
        printStacktrace(t, Level.Error, -1);
    }
    
    public static void printStacktrace(Throwable t, int length) {
        printStacktrace(t, Level.Error, length);
    }

    public static ArrayList<String> getLog() {
        return logger.log;
    }

    public static void writeLine() {
        print("", Level.None);
    }

    public enum Level {

        None, Info, Debug, Warn, Error;
    }

    @FunctionalInterface
    public interface LogAction {

        public void newLine(String s);
    }
}
