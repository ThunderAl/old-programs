package ru.alexp.util;

import java.io.File;
import java.nio.file.Path;
import javax.swing.JOptionPane;

/**
 *
 * @author Александр
 */
public class Dir {

    private final String sysenv;
    private final String subdir;

    private static Dir istanse;

    private Dir(String sysenv, String subdir) {
        this.subdir = subdir;
        this.sysenv = sysenv;
    }

    public File getApplicationDir() {

        String AppData = System.getenv(sysenv);
        String MainDir = subdir;
        File AppDir;

        if (AppData != null) {

            AppDir = new File(AppData, MainDir);

        } else {

            AppDir = new File(System.getProperty("user.home", "."), MainDir);
        }

        return AppDir;
    }

    public void testApplicationDir() {

        File f = getApplicationDir();

        if (!f.exists()) {

            f.mkdirs();
        }

        if (!f.canRead()) {

            JOptionPane.showMessageDialog(null, "Невозможно прочитать папку:\n\"" + f.getPath() + "\"\nОбратитесь с этой проблемой к администратору вашего компьютера!", "Ошибка!", JOptionPane.ERROR_MESSAGE);
            Logger.error("File Error!");
            System.exit(0);

        } else if (!f.canWrite()) {

            JOptionPane.showMessageDialog(null, "Невозможно использовать папку:\n\"" + f.getPath() + "\"\nОбратитесь с этой проблемой к администратору вашего компьютера!", "Ошибка!", JOptionPane.ERROR_MESSAGE);
            Logger.error("File Error!");
            System.exit(0);
        }
    }

    public Path relativize(Path path) {

        return getApplicationDir().toPath().relativize(path);
    }

    public String getFileName(File f) {

        final String[] filenamesplit = f.getAbsolutePath().split(File.separator.equals("\\") ? File.separator + File.separator : File.separator);
        final int filenamelen = filenamesplit.length - 1;

        return filenamesplit[filenamelen];
    }

    public static void createAppDir(String sysenv, String subdir) {
        istanse = new Dir(sysenv, subdir);
        istanse.getApplicationDir().mkdirs();
    }

    public static Dir getIstanse() {
        if (istanse == null) {
            throw new RuntimeException("Application dir is not initialized.");
        }
        return istanse;
    }
}
