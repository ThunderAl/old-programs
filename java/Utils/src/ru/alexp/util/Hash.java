package ru.alexp.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Александр
 */
public class Hash {

    public static String md5(String Password) {

        try {

            MessageDigest m = MessageDigest.getInstance("MD5");

            m.reset();
            m.update(Password.getBytes());

            byte[] digest = m.digest();

            BigInteger bigInt = new BigInteger(1, digest);
            String hash = bigInt.toString(16);

            while (hash.length() < 32) {

                hash = "0" + hash;
            }

            return hash;

        } catch (NoSuchAlgorithmException e) {

            return null;
        }
    }

    public static String md5(File file) {

        InputStream is = null;

        try {

            MessageDigest digest = MessageDigest.getInstance("MD5");

            is = new FileInputStream(file);

            byte[] buffer = new byte[8192];

            int read;

            while ((read = is.read(buffer)) > 0) {

                digest.update(buffer, 0, read);

            }

            byte[] md5sum = digest.digest();
            BigInteger bigInt = new BigInteger(1, md5sum);
            String output = bigInt.toString(16);

            while (output.length() < 32) {

                output = "0" + output;
            }

            return output;

        } catch (IOException | NoSuchAlgorithmException e) {

            throw new RuntimeException("Unable to process file for MD5", e);

        } finally {

            if (is != null) {
                
                try {

                    is.close();

                } catch (IOException e) {

                    throw new RuntimeException("Unable to close input stream for MD5 calculation", e);

                }
            }
        }
    }
}
