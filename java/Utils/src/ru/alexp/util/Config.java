package ru.alexp.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import ru.alexp.util.json.JSONException;
import ru.alexp.util.json.JSONObject;

/**
 *
 * @author Александр
 */
public class Config {

    private static File cfgFile = new File(Dir.getIstanse().getApplicationDir(), "conf.json");

    private static JSONObject cfg = new JSONObject();

    public static void load() {
        if (!cfgFile.exists()) {
            return;
        }

        final StringBuilder sb = new StringBuilder();

        try {
            try (BufferedReader reader = new BufferedReader(new FileReader(cfgFile.getAbsoluteFile()))) {
                reader.lines().forEach(line -> {
                    sb.append(line).append('\n');
                });
            }
        } catch (IOException e) {
            Logger.error("Невозможно прочитать файл!");
            Logger.error(e.getMessage());
        }

        try {
            if (sb.length() > 2) {
                cfg = new JSONObject(sb.toString());
            }
        } catch (JSONException e) {
            Logger.warn("Config loading error: \"" + e.getMessage() + "\".");
            Logger.warn("Reloading configs ...");
            clear();
            save();
            Logger.warn("Configs cleared and reloaded.");
        }
    }

    public static void save() {
        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(cfgFile.getAbsoluteFile()))) {
                writer.write(cfg.toString(Vars.isDebug() ? 4 : 0));
            }
        } catch (IOException e) {
            Logger.error("Невозможно записать в файл!");
            Logger.printStacktrace(e);
        }
    }

    public static void put(String key, Object value) {
        try {
            cfg.put(key, value);
        } catch (JSONException e) {
            Logger.warn("JSON error: " + e.getMessage());
        }
    }

    public static Object get(String key, Object def) {
        Object JSONreturn = def;
        if (cfg.has(key)) {
            try {
                JSONreturn = cfg.get(key);
            } catch (JSONException unimportant) {
            }
        }
        return JSONreturn;
    }

    public static Object get(String key) {
        return get(key, null);
    }

    public static boolean contains(String key) {
        return cfg.has(key);
    }

    public static void clear() {
        cfg = new JSONObject();
    }

    public static void delete(String key) {
        if (contains(key)) {
            cfg.remove(key);
        }
    }

    public static JSONObject getMap() {
        return cfg;
    }

    public static void setConfigFile(File f) {
        cfgFile = f;

    }
}
