package ru.alexp.game.server.util;

/**
 *
 * @author Alex_P
 */
public class Logger {

    private static Logger logger = null;

    public static Logger getLogger() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }
}
