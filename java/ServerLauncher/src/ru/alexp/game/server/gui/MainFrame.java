package ru.alexp.game.server.gui;

import com.alee.extended.layout.HorizontalFlowLayout;
import com.alee.laf.label.WebLabel;
import com.alee.laf.panel.WebPanel;
import com.alee.laf.rootpane.WebFrame;
import com.alee.laf.scroll.WebScrollPane;
import com.alee.laf.text.WebTextArea;
import com.alee.laf.text.WebTextField;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import ru.alexp.game.server.main;
import ru.alexp.game.server.util.CommandManager;

/**
 *
 * @author Alex_P
 */
public class MainFrame extends WebFrame {

    private static MainFrame frame = null;

    private final WebTextArea logTextArea = new WebTextArea();
    private final WebScrollPane logScrollPane = new WebScrollPane(null);

    private final WebTextField commandField = new WebTextField();

    public MainFrame() {
        setSize(650, 450);
        setResizable(true);
        setLocationRelativeTo(null);
        setIconImage(new ImageIcon(this.getClass().getResource("/favicon.png")).getImage());
        setRound(2);
        setShadeWidth(25);

        logScrollPane.setViewportView(logTextArea);
        logScrollPane.setDrawBorder(false);
        logScrollPane.setDrawFocus(false);
        logTextArea.setMargin(0, 15, 0, 15);
        logTextArea.setEditable(false);
        logTextArea.setBackground(Color.BLACK);
        logTextArea.setForeground(Color.WHITE);
        logTextArea.setFontName("Courier New");
        logTextArea.setFontSize(logTextArea.getFontSize() + 3);

        final WebPanel commandPanel = new WebPanel();
        final WebLabel commandPreffix = new WebLabel(">");
        commandPreffix.setForeground(logTextArea.getForeground());
        commandPreffix.setFontName(logTextArea.getFontName());
        commandPreffix.setFontSize(logTextArea.getFontSize() + 2);
//        commandPreffix.setBoldFont();
        commandField.setDrawBorder(false);
        commandField.setDrawFocus(false);
        commandField.setDrawShade(false);
        commandField.setForeground(logTextArea.getForeground());
        commandField.setFontName(logTextArea.getFontName());
        commandField.setFontSize(logTextArea.getFontSize());
//        commandField.setBoldFont();
        commandPanel.setBackground(logTextArea.getBackground());
        commandPanel.setMargin(logTextArea.getMargin());
        commandPanel.setLayout(new HorizontalFlowLayout(5, true));
        commandPanel.add(commandPreffix);
        commandPanel.add(commandField);

        final GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addComponent(logScrollPane)
                                .addComponent(commandPanel)
                        )
                )
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(logScrollPane)
                        .addComponent(commandPanel, 25, 25, 25)
                )
        );

        commandField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        CommandManager.onCommand(0, commandField.getText());
                    } catch (Exception e) {

                    }
                    addLogLine(commandField.getText());
                    commandField.setText("");
                }
            }
        });

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                main.exit();
            }
        });
    }

    public void addLogLine(String line) {

        final int cut = 1024;
        int i = logTextArea.getText().length();

        if (i == 0) {
            logTextArea.setText(line);
        } else if (i < cut) {
            logTextArea.setText(logTextArea.getText() + "\n" + line);
        } else {
            logTextArea.setText(logTextArea.getText().substring(i - cut) + "\n" + line);
        }
        
        logTextArea.select(i + 1, i + 1);
    }

    public static MainFrame getFrame() {
        if (frame == null) {
            frame = new MainFrame();
        }
        return frame;
    }
}
