package ru.alexp.game.server;

import com.alee.laf.WebLookAndFeel;
import ru.alexp.game.server.gui.MainFrame;

/**
 *
 * @author Alex_P
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        WebLookAndFeel.install();
        WebLookAndFeel.setDecorateAllWindows(true);
        
        MainFrame.getFrame().setVisible(true);
    }

    public static void exit() {
        System.exit(0);
    }
}
