package ru.alexp.tools;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import ru.alexp.util.Logger;

/**
 *
 * @author Alex_P
 */
public class socsrv {

    private static ServerSocket server;
    public static boolean isRun = true;
    public static ArrayList<Client> clients = new ArrayList();

    public static void main(String[] args) {
        Logger.setupLogger();
        cerateServer(5037);
        while (isRun) {
            try {
                Client client = new Client(server.accept());
                clients.add(client);
                client.run();
            } catch (IOException e) {
                Logger.error(e.getMessage());
                System.exit(1);
            }
        }
    }

    private static void cerateServer(int port) {
        try {
            server = new ServerSocket(port);
            Auth.loadParameters();
            Logger.info("Sever started in " + server.getLocalPort() + " port.");
        } catch (IOException ex) {
            cerateServer(port + 1);
        }
    }
}
