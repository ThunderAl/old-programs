package ru.alexp.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Arrays;
import ru.alexp.util.Logger;

/**
 *
 * @author Alex_P
 */
public final class Client {

    private final Socket client;
    private final BufferedWriter out;
    private final BufferedReader in;
    private final Auth auth;
    private boolean isLogined = false;
    private String name = "";

    public Client(Socket client) throws IOException {
        this.client = client;
        name = getSocket().getLocalAddress().getHostAddress();
        out = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
        in = new BufferedReader(new InputStreamReader(getSocket().getInputStream()));
        auth = new Auth();
    }

    public void run() {
        Logger.info("Client " + getSocket().getLocalAddress().getHostAddress() + " connected.");
        sendLine("OK");
        Thread readerThread = new Thread(() -> {
            while (!getSocket().isClosed()) {
                try {
                    String line = in.readLine();
                    if (line != null) {
                        onCommand(line);
                    }
                } catch (IOException e) {
                    Logger.info("Client " + getSocket().getLocalAddress().getHostAddress() + " disconnected.");
                    try {
                        getSocket().close();
                    } catch (IOException ex) {
                    }
                }
            }
        });
        readerThread.setDaemon(true);
        readerThread.start();
    }

    private void onCommand(String command) {
        
        Logger.info(name + " >> " + command);

        if ((command.startsWith("login") && command.contains(" "))) {
            try {
                String[] com = command.split(" ");
                if (auth.auth(com[1], com[2])) {
                    name = com[1];
                    Logger.info(getSocket().getLocalAddress().getHostAddress() + " logined as " + name);
                    sendLine("Logined as " + name);
                    isLogined = true;
                }
            } catch (Exception e) {
                sendLine("Syntax reeor.");
            }
        } else if (!isLogined) {
            sendLine("needlogin");
        }
    }

    public Socket getSocket() {
        return client;
    }

    public boolean sendLine(String line) {
        try {
            out.write(line + "\n");
            out.flush();
            return true;
        } catch (IOException e) {
            return false;
        }
    }
}
