package ru.alexp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import ru.alexp.util.Logger;
import ru.alexp.util.json.JSONException;
import ru.alexp.util.json.JSONObject;

/**
 *
 * @author Alex_P
 */
public class Auth {
    
    private static JSONObject users;

    public static void loadParameters() {
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(new File(Auth.class.getProtectionDomain().getCodeSource().getLocation().toURI().normalize().getPath(), "users.json")));
            final StringBuilder sb = new StringBuilder();
            
            while (reader.ready()) {
                sb.append(reader.readLine()).append("\n");
            }
            
            users = new JSONObject(sb.toString());
            
        } catch (IOException | NullPointerException | JSONException | URISyntaxException e) {
            Logger.error(e.getMessage());
        }
    }
    
    public boolean auth(String login, String password) {
        if (users == null) {
            loadParameters();
        }
        return users.has(login) &&
                (users.getString(login).equalsIgnoreCase(password.toLowerCase()) || users.getString(login).equalsIgnoreCase("null"));
    }
}
