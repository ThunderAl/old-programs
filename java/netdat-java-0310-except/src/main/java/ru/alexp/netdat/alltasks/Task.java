package ru.alexp.netdat.alltasks;

public interface Task {
    void run() throws Exception;
}
