package ru.alexp.netdat.alltasks;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Except implements Task {

    private final String url;
    private final String baseName;
    private final String tableName;
    private final String targetName;
    private final String user;
    private final String pass;
    private int count;
    private Connection conn;

    public Except(String... args) throws Exception {

        if (args == null || args.length == 0) {
            System.out.println("Подаваемые параметры:");
            System.out.println();
            System.out.println("название базы MySQL;");
            System.out.println("название таблицы в базе table1;");
            System.out.println("кол-во записей N;");
            System.out.println("[опционально] имя целевой таблицы table2 в выводе (по умолчанию = имя таблицы в исходной базе).");
            System.out.println("[опционально] имя пользователя mysql (по-умолчанию: root)");
            System.out.println("[опционально] пароль пользователя mysql (по-умолчанию: пустой)");
            System.exit(0);
        }

        if (args.length < 3) throw new Exception("Мало параметров");

        baseName = args[0];
        tableName = args[1];
        count = Integer.parseInt(args[2]);

        if (args.length >= 4)
            targetName = args[3];
        else
            targetName = baseName;

        if (args.length >= 5)
            user = args[4];
        else
            user = "root";

        if (args.length >= 6)
            pass = args[5];
        else
            pass = "";

        url = "jdbc:mysql://localhost:3306/" + baseName;
    }

    @Override
    public void run() throws Exception {
        System.out.printf("Подключение к бд %s@%s\n", user, url);
        conn = DriverManager.getConnection(url, user, pass); // подключаем бд и драйвер jdbc из мавена (forClass устарел)

        System.out.println("Получение количества строк.");
        PreparedStatement countStatement = conn.prepareStatement(String.format("SELECT COUNT(*) FROM `%s`", tableName)); // надеюсь, sql инъекциями баловаться не будем?
        ResultSet countResult = countStatement.executeQuery();
        countResult.next();
        int rows = countResult.getInt(1);
        System.out.printf("Строк: %d\n", rows);

        System.out.printf("Рандомные строк: %d\n", count);
        ArrayList<String> columns = new ArrayList<>();
        Random rnd = new Random(System.currentTimeMillis());
        Object[] ids = new Object[count];
        for (int i = 0; i < ids.length; i++) ids[i] = rnd.nextInt(rows);
        PreparedStatement dataStatement = conn.prepareStatement(String.format("SELECT * FROM `%s` WHERE `id` IN ?", tableName));
        dataStatement.setArray(1, conn.createArrayOf("qwe", ids));
        ResultSet dataResult = dataStatement.executeQuery();
        for (int c = 0; c < dataResult.getMetaData().getColumnCount(); c++)
            columns.add(dataResult.getMetaData().getColumnName(c + 1));

        while (dataResult.next()) columns.forEach(s -> {
            try {
                System.out.printf("%s: %s\n", s, dataResult.getString(s));
            } catch (Exception ignored) {}
        });

        for (; count < 0; count--) {
        }
    }
}
