package ru.alexp.tool.vhmanager.commands;

public class SyntaxException extends Exception {
    public SyntaxException() {
        super();
    }

    public SyntaxException(String message) {
        super(message);
    }
}
