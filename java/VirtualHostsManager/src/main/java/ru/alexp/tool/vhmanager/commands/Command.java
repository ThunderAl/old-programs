package ru.alexp.tool.vhmanager.commands;

public abstract class Command {

    protected CommandManager commandManager;

    public abstract String getName();

    public String getDescr() {
        return null;
    }

    public String getSyntax() {
        return null;
    }

    public String[] getAliases() {
        return null;
    }

    public abstract void onCommand(String... args) throws SyntaxException;

    public Command setCommandManager(CommandManager cm) {
        commandManager = cm;
        return this;
    }
}
