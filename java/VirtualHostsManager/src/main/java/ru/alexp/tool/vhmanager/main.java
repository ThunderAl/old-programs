package ru.alexp.tool.vhmanager;


import ru.alexp.tool.vhmanager.commands.CommandManager;
import ru.alexp.tool.vhmanager.commands.Help;

import java.util.Arrays;
import java.util.Calendar;

public class main {

    private static final CommandManager cm = new CommandManager();

    public static void main(String[] args) {
        registerCommands();
        printGreeting();

        if (args == null || args.length == 0)
            cm.invokeCommand("help");
        else if (args.length == 1)
            cm.invokeCommand(args[0]);
        else
            cm.invokeCommand(args[0], Arrays.copyOfRange(args, 1, args.length));
    }

    private static void printGreeting() {
        System.out.println("git  client");
        System.out.println("(c) Alex_P " + Calendar.getInstance().get(Calendar.YEAR));
        System.out.println();
    }

    private static void registerCommands() {
        cm.addCommand(new Help());
    }
}
