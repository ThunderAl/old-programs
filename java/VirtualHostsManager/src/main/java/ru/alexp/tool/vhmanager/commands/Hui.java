package ru.alexp.tool.vhmanager.commands;

import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.time.LocalTime;

import static java.lang.System.out;

public class Hui extends Application {

    final HBox hb = new HBox();
    private final ObservableList<ImaginaryProcess> data =
            FXCollections.observableArrayList();
    private TableView<ImaginaryProcess> table = new TableView<>();

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage stage) {
        Scene scene = new Scene(new Group());
        stage.setTitle("Имитатор процессов");
        stage.setWidth(640);
        stage.setHeight(500);

        final Label label = new Label("Список процессов");

        table.setEditable(false);

        TableColumn id_col = new TableColumn("id");
        id_col.setCellValueFactory(param -> {
            System.out.println("говно");
            return new SimpleStringProperty("пиздец");
        });
        TableColumn cp_col = new TableColumn("ЦП");
        TableColumn mem_col = new TableColumn("ОЗУ");
        TableColumn added_col = new TableColumn("добавлен");

        table.setItems(data);
        table.getColumns().addAll(id_col, cp_col, mem_col, added_col);

        final TextField add_id = new TextField();
        add_id.setPromptText("id");
        final TextField add_cp = new TextField();
        add_cp.setPromptText("ЦП");
        final TextField add_mem = new TextField();
        add_mem.setPromptText("Память");

        final Button add_button = new Button("Добавить");
        add_button.setOnAction(event -> {
            data.add(new ImaginaryProcess(
                    Integer.parseInt(add_id.getText()),
                    Integer.parseInt(add_cp.getText()),
                    Integer.parseInt(add_mem.getText())
            ));
            //out.println(add_id.getText());
            add_id.clear();
            add_cp.clear();
            add_mem.clear();
        });

        hb.getChildren().addAll(add_id, add_cp, add_mem, add_button);
        hb.setSpacing(1);

        final VBox vbox = new VBox();
        vbox.setSpacing(2);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table, hb);

        ((Group) scene.getRoot()).getChildren().addAll(vbox);

        stage.setScene(scene);
        stage.show();
    }

    public static class ImaginaryProcess {
        private final int id;
        private final int cp_use;
        private final int mem_use;
        private LocalTime created = LocalTime.now();

        private ImaginaryProcess(int _id, int _cp_use, int _mem_use) {
            this.id = (_id);
            this.cp_use = (_cp_use);
            this.mem_use = (_mem_use);
        }

        public int getId() {
            return id;
        }
    }
}