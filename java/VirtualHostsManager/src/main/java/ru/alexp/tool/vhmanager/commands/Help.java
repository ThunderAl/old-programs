package ru.alexp.tool.vhmanager.commands;

public class Help extends Command {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescr() {
        return "Gets this help.";
    }

    @Override
    public String[] getAliases() {
        return new String[]{"h", "?"};
    }

    @Override
    public void onCommand(String... args) {
        for (String line : commandManager.getHelp()) System.out.println(line);
    }
}
