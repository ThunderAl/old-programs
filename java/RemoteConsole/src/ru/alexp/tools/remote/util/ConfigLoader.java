package ru.alexp.tools.remote.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import ru.alexp.util.Logger;
import ru.alexp.util.Vars;
import ru.alexp.util.json.JSONObject;

/**
 *
 * @author Александр
 */
public class ConfigLoader {

    public static void loadConfig(File conf) {
        Logger.info("Loading config from " + conf.getPath() + " ...");
        
        try {
            final BufferedReader reader = new BufferedReader(new FileReader(conf));

            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }

            final JSONObject json = new JSONObject(sb.toString());
            Logger.info("Config params:");
            json.keys().forEachRemaining((String key) -> {
                Vars.put(key, json.get(key));
                Logger.info("    " + key + ": " + json.getString(key));
            });
            Logger.writeLine();
        } catch (IOException ex) {
            Logger.error(ex.getMessage());
        }
    }
}
