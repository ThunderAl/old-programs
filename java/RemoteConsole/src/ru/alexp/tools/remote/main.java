package ru.alexp.tools.remote;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import ru.alexp.tools.remote.util.ConfigLoader;
import ru.alexp.util.Logger;
import ru.alexp.util.Vars;

/**
 *
 * @author Александр
 */
public class main {

    private static String[] startupArgs;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Logger.none("#include <AlexPRemoteConsole>");
        Logger.none("         <Version   0.0.1   >");
        Logger.writeLine();

        // startup args
        startupArgs = args;

        if (startupArgs.length == 0) {
            Logger.error("no params -- no process");
        } else {
            Logger.info("Sartup params:");
            for (int i = 0; startupArgs.length > i; i++) {
                if (startupArgs[i].startsWith("-")) {
                    String key = startupArgs[i].replace("-", "");
                    Object value;
                    if (startupArgs.length > (i + 1) && !startupArgs[(i + 1)].startsWith("-")) {
                        value = startupArgs[(i + 1)];
                    } else {
                        value = true;
                    }
                    Vars.put(key, value);
                    Logger.info("    " + key + ": " + value);
                }
            }
            Logger.writeLine();
        }

        if (Vars.containsKey("conf")) {
            ConfigLoader.loadConfig(new File((String) Vars.get("conf")));
        }

        final ProcessBuilder pb = new ProcessBuilder();
        final ArrayList<String> launchParams = new ArrayList<>();

        launchParams.add((String) Vars.get("file"));

        final String[] params = ((String) Vars.get("params")).split(" ");

        for (String str : params) {
            launchParams.add(str);
        }

        Logger.info("Execution line: " + launchParams.toString());

        pb.directory(new File((String) Vars.get("currentdir")));

        pb.command(launchParams);

        pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        pb.redirectError(ProcessBuilder.Redirect.INHERIT);
//        pb.redirectInput(ProcessBuilder.Redirect.INHERIT);

        final Process proc = pb.start();

        final BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        if (in.readLine().equals("kill")) {
            proc.destroy();
        }
    }

}
