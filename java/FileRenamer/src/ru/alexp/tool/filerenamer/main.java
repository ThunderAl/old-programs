package ru.alexp.tool.filerenamer;

import java.io.File;
import java.util.Arrays;

/**
 *
 * @author Александр
 */
public class main {

    private static final String path = "J:\\Video\\MLP\\S3";

    private static final String[] names = new String[14];
    private static final String[] sub = new String[14];

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] array = new File(path).list((File dir, String name) -> {
            return name.endsWith(".mkv");
        });
        for (String row : array) {
            names[Integer.parseInt(row.replaceAll("^Episode (.{1,2}) \\(.*\\)$", "$1"))] = row.substring(0, row.length() - 4);
        }
        System.out.println(Arrays.toString(names));

        String[] subs = new File(path).list((File dir, String name) -> {
            return name.endsWith(".ass");
        });
        for (String row : subs) {
            sub[Integer.parseInt(row.replaceAll("^.*S03E(.{1,2})_.*$", "$1"))] = row.substring(0, row.length() - 4);
        }
        System.out.println(Arrays.toString(sub));

        for (int i = 1; i < names.length; i++) {
            new File(path, sub[i] + ".ass").renameTo(new File(path, names[i] + ".ass"));
        }
    }

}
