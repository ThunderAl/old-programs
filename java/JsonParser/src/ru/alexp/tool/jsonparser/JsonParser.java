package ru.alexp.tool.jsonparser;

import java.util.Calendar;
import javax.swing.JFrame;
import ru.alexp.tool.jsonparser.util.Logger;
import ru.alexp.tool.jsonparser.util.Vars;

/*
 *
 * @author Александр
 */
public class JsonParser {
    
    private static String[] startupArgs;
    private static JFrame frame;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Create log
        Logger.setupLogger();

        // startup args
        startupArgs = args;
        if (startupArgs.length > 0) {
            for (int i = 0; startupArgs.length > i; i++) {
                if (startupArgs[i].startsWith("-")) {
                    String key = startupArgs[i].replace("-", "");
                    Object value;
                    if (startupArgs.length > (i + 1) && !startupArgs[(i + 1)].startsWith("-")) {
                        value = startupArgs[(i + 1)];
                    } else {
                        value = true;
                    }
                    Vars.put(key, value);
                }
            }
        }
        
        Logger.none("-=@ Json parser @=-");
        Logger.none("  @ Alex_P " + Calendar.getInstance().get(Calendar.YEAR) + " @\n");
        Logger.info("Loding frame.");
        try {
            frame = new MainFrame();
            frame.setVisible(true);
        } catch (Exception e) {
            Logger.error("Error:");
            Logger.error(e.getMessage());
            System.exit(1);
        } finally {
            Logger.info("Loding done.");
        }
    }

}
