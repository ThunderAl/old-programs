package ru.alexp.tool.jsonparser.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;

/**
 *
 * @author Александр
 */
public class Net {

    public static final String POST_method = "POST";
    public static final String GET_method = "GET";

    public static String query(String url, String parameters, String method) {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(url).openConnection();

            if (parameters != null) {
                connection.setRequestMethod(method);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", Integer.toString(parameters.getBytes().length));
            }
            connection.setRequestProperty("Content-Language", "en-EN");
            connection.setRequestProperty("User-Agent", "GaurangaCraftLauncher");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(10000);
            connection.connect();

            if (parameters != null) {
                try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
                    wr.writeBytes(parameters);
                    wr.flush();
                }
            }

            InputStream is = connection.getInputStream();
            StringBuilder response;

            try (BufferedReader rd = new BufferedReader(new InputStreamReader(is))) {
                response = new StringBuilder();
                String line;
                boolean firstline = true;

                while ((line = rd.readLine()) != null) {
                    response.append(firstline ? "" : '\n');
                    response.append(line);
                    firstline = false;
                }
            }

            return response.toString();

        } catch (IOException e) {
            Logger.error("Query error: " + e.getMessage());
            e.printStackTrace();
            return null;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static String query(String url, String parameters) {
        return query(url, parameters, POST_method);
    }

    public static String query(String url) {
        return query(url, null, POST_method);
    }
}
