package ru.alexp.tool.jsonparser.util;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Alex_P
 */
public class Logger {

    private static Logger logger;
    private final ArrayList<String> log = new ArrayList();
    private static final ArrayList<LogAction> hooks = new ArrayList();

    public static void setupLogger() {

        if (logger != null) {
            debug("Setting up new logger!");
        }

        logger = new Logger();
    }

    public static void addHook(LogAction r) {
        hooks.add(r);
    }

    private static void print(String s, Level level) {
        
        if (level == Level.Debug && !Vars.isDebug()) {
            return;
        }
        
        StringBuilder echo = new StringBuilder();

        echo.append(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
        echo.append(":");
        echo.append(Calendar.getInstance().get(Calendar.MINUTE));

        if (level != Level.None) {
            echo.append(" ");
            echo.append("[" + level.toString().toUpperCase() + "]");
        }

        echo.append(" ");
        echo.append(s);

        if (level == Level.Error) {
            System.err.println(echo.toString());
        } else {
            System.out.println(echo.toString());
        }

        logger.log.add(echo.toString());

        if (!hooks.isEmpty()) {
            for (LogAction action : hooks) {
                action.newLine(echo.toString());
            }
        }

    }

    public static void none(String s) {
        print(s, Level.None);
    }

    public static void info(String s) {
        print(s, Level.Info);
    }

    public static void debug(String s) {
        print(s, Level.Debug);
    }

    public static void warn(String s) {
        print(s, Level.Warn);
    }

    public static void error(String s) {
        print(s, Level.Error);
    }

    public static void clear() {
        logger.log.clear();
    }

    public static ArrayList<String> getLog() {
        return logger.log;
    }

    public static void writLine() {
        print("", Level.None);
    }

    public enum Level {

        None, Info, Debug, Warn, Error;
    }

    @FunctionalInterface
    public interface LogAction {

        public void newLine(String s);
    }
}
