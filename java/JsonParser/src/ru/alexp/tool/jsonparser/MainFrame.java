package ru.alexp.tool.jsonparser;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import ru.alexp.tool.jsonparser.json.JSONArray;
import ru.alexp.tool.jsonparser.json.JSONException;
import ru.alexp.tool.jsonparser.json.JSONObject;

/**
 *
 * @author Александр
 */
public class MainFrame extends JFrame {

    private final JTextArea input;
    private final JTextArea output;
    private final JCheckBox min;
    private final JCheckBox edit;

    private JSONObject json = new JSONObject();
    private boolean isMin = false;
    private boolean isRaw = false;

    public MainFrame() {

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            }
        }

        input = new JTextArea();
        output = new JTextArea();
        min = new JCheckBox("Min");
        edit = new JCheckBox("Edit raw JSON");

        output.setEditable(false);

        JScrollPane inputPane = new JScrollPane();
        inputPane.setViewportView(input);

        JScrollPane outputPane = new JScrollPane();
        outputPane.setViewportView(output);

        setSize(900, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(true);
        setTitle("Json Parser");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(3)
                                        .addComponent(edit)
                                        .addGap(0, 0, Short.MAX_VALUE)
                                )
                                .addComponent(inputPane, 0, 0, Short.MAX_VALUE)
                        )
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(3)
                                        .addComponent(min)
                                        .addGap(0, 0, Short.MAX_VALUE)
                                )
                                .addComponent(outputPane, 0, 0, Short.MAX_VALUE)
                        )
                )
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(3)
                                .addComponent(edit)
                                .addGap(3)
                                .addComponent(inputPane)
                        )
                        .addGroup(layout.createSequentialGroup()
                                .addGap(3)
                                .addComponent(min)
                                .addGap(3)
                                .addComponent(outputPane)
                        )
                )
        );

        input.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                update();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                update();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                fromStringToJson();
            }
        });

        min.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                update();
                isMin = min.isSelected();
            }
        });

        edit.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                isRaw = edit.isSelected();
                if (isRaw) {
                    input.setText(new JSONObject(json.toString()).toString(4));
                } else {
                    decodeJson();
                }
                update();
            }
        });
    }

    private void update() {
        if (isRaw) {
            checkJson();
        } else {
            fromStringToJson();
        }
    }

    private void decodeJson() {
        try {
            final JSONArray jarray = json.getJSONArray("reports");
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < jarray.length(); i++) {
                final JSONObject item = jarray.getJSONObject(i);
                sb.append(item.getString("title")).append("\n");
                try {
                    final JSONArray errs = item.getJSONArray("errors");
                    for (int j = 0; j < errs.length(); j++) {
                        sb.append(errs.get(j)).append(" ");
                    }
                    sb.append("\n");
                } catch (JSONException e) {
                    sb.append(e.getMessage());
                }
                sb.append(item.getString("url")).append("\n\n");
            }
            input.setText(sb.toString());
        } catch (JSONException e) {
            output.setText(e.getMessage());
        }
    }

    private void checkJson() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String rawjson = input.getText();
                try {
                    json = new JSONObject(rawjson);
                    updateOutput();
                } catch (JSONException e) {
                    output.setText(e.getMessage());
                }
            }
        }).start();
    }

    private void fromStringToJson() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                json = new JSONObject();
                final JSONArray array = new JSONArray();
                final String[] list = input.getText().split("\\n\\n");
                for (String item : list) {
                    final JSONObject it = new JSONObject();
                    final String[] split = item.split("\\n");
                    if (split.length > 2) {
                        it.put("title", split[0].trim()); // название
                        it.put("errors", split[1].trim().split("\\s")); // ошибки
                        it.put("url", split[2].trim()); // урл
                    }
                    array.put(it);
                }
                json.put("reports", array);
                updateOutput();
            }
        }).start();
    }

    private void updateOutput() {
        String out = "";
        if (isMin) {
            out = new JSONObject(out).toString();
        } else {
            out = new JSONObject(out).toString(4);
        }
        output.setText(out);
    }

}
