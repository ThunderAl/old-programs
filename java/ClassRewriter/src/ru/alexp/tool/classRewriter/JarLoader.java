package ru.alexp.tool.classRewriter;
 
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
 
/**
 *
 * @author Александр
 *
 * код взят отсюда [url]http://samolisov.blogspot.ru/2008/01/java.html[/url]
 */
public class JarLoader extends ClassLoader {
 
    private final ArrayList<File> jarlist;
    private final Map<String, Class<?>> classes;
    private final Map<String, JarFile> jarclasses; // classname, jarfile

 
    public JarLoader(ArrayList<File> jarlist) {
        this.jarlist = jarlist;
        classes = new HashMap<>();
        jarclasses = new HashMap<>();
    }
 
    /**
     * Ф-ция загружает все классы из указанных jar.
     *
     * @throws Exception
     */
    private void loadJarClasses() throws Throwable {
        for (File f : jarlist) {
            loadJar(f);
        }
    }
 
    /**
     * Начать процесс загрузки классов из jar в спике.
     */
    public void load() {
        try {
            updateJarClasses(); // загружаем списки всех классов
            loadJarClasses(); // загружаем классы
        } catch (Throwable e) {
            // если что-то не загрузилось
        }
    }
 
    /**
     * Создаёт спикок "какой класс где лежит".
     *
     * @throws Exception
     */
    private void updateJarClasses() throws Exception {
        for (File f : jarlist) {
            loadJarClasses(f);
        }
    }
 
    private void loadJarClasses(File jar) throws Exception {
        final JarFile jarFile = new JarFile(jar);
        Enumeration entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            final JarEntry jarEntry = (JarEntry) entries.nextElement();
            if (jarEntry.getName().endsWith(".class")) {
                final String className = jarEntry.getName().replace("/", ".").replace(".class", "");
                jarclasses.put(className, jarFile);
            }
        }
    }
 
    private void loadJar(File jar) throws Throwable {
        final JarFile jarFile = new JarFile(jar);
        Enumeration entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            final JarEntry jarEntry = (JarEntry) entries.nextElement();
            if (jarEntry.getName().endsWith(".class")) {
                final String className = jarEntry.getName().replace("/", ".").replace(".class", "");
                final byte[] data = loadClassData(jarFile, jarEntry);
                if (data != null && !classes.containsKey(className)) {
                    System.out.println("Loading " + className);
                    try {
                        Class<?> clazz = defineClass(className, data, 0, (int) jarEntry.getSize());
                        classes.put(className, clazz);
                    } catch (NoClassDefFoundError e) {
                        final String classname = e.getMessage().replace("/", ".");
                        loadClass(jarclasses.get(classname), classname); // загружаем необходимый класс
                        loadClass(jarclasses.get(className), className); // пытаемся загрузить старый
                    }
                }
            }
        }
    }
 
    private void loadClass(JarFile file, String className) throws Throwable {
        System.out.println("Loading " + className);
        try {
            final JarEntry entry = file.getJarEntry(className.replace(".", "/") + ".class");
            final byte[] data = loadClassData(file, entry);
            if (data != null) {
                Class<?> clazz = defineClass(className, data, 0, (int) entry.getSize());
                classes.put(className, clazz);
            }
        } catch (NoClassDefFoundError e) {
            final String classname = e.getMessage().replace("/", ".");
            loadClass(jarclasses.get(classname), classname);
        }
    }
 
    @Override
    public synchronized Class<?> loadClass(String name) throws ClassNotFoundException {
 
        Class<?> result = classes.get(name);
 
        // Если класса нет в кэше то возможно он системный
        if (result == null) {
            result = super.findSystemClass(name);
        }
 
        return result;
    }
 
    private byte[] loadClassData(JarFile jarFile, JarEntry jarEntry) throws IOException {
 
        long size = jarEntry.getSize();
        if (size == -1 || size == 0) {
            return null;
        }
 
        byte[] data = new byte[(int) size];
        InputStream in = jarFile.getInputStream(jarEntry);
        in.read(data);
 
        return data;
    }
 
    /**
     * Освобождаем память, занимаемую под списки классов.
     */
    public void clear() {
        jarlist.clear();
        classes.clear();
        jarclasses.clear();
    }
}