package ru.alexp.tool.classRewriter;

/**
 *
 * @author Александр
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length < 2) {
            if (args.length == 0 || (args.length > 0 && args[0].equalsIgnoreCase("help"))) {
                System.out.println("ClassRenamer.jar <injectClasses.jar> <mainClasses.jar[;lib1.jar[;lib2.jar[;...]]]> [mainClass]");
            }
        } else {
            Vars.put("injectJar", args[0]);
            if (args[1].contains(";")) {
                parseLibs(args);
            } else {
                Vars.put("mainJar", args[1]);
            }
            
        }
    }

    private static void parseLibs(String[] args) {
        final String[] jars = args[1].split(";");
        final String[] libs = new String[jars.length - 1];
        for (int i = 1; i < jars.length; i++) {
            libs[i - 1] = jars[i];
        }
        Vars.put("mainJar", jars[0]);
        Vars.put("libJars", libs);
    }
}
