package ru.alexp.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Base64;
import ru.alexp.util.Logger;

/**
 *
 * @author Alex_P
 */
public class main {

    private static String host;
    private static int port;

    private static Socket client;

    private static boolean isRun = true;

    public static void main(String[] args) {
        Logger.setupLogger();

        try {
//            BufferedReader consoleInput = new BufferedReader(new InputStreamReader(System.in));
//            Logger.print("Host (localhost): ", Logger.Level.Info, false);
//            host = consoleInput.readLine();
//            if (host.equals("")) {
                host = "smtp.yandex.ru";
//            }
//            Logger.print("Port (370): ", Logger.Level.Info, false);
//            try {
//                port = Integer.parseInt(consoleInput.readLine());
//            } catch (NumberFormatException e) {
                port = 993;
//            }
//            Logger.writeLine();
            Logger.info("Try to connect.");

            client = new Socket(host, port);
            if (client.isConnected()) {
                Logger.info("Connected to " + host + ":" + port);
                Logger.info("To disconnect tupe \"disconnect\"");
            }
            final BufferedWriter socketWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
            final BufferedReader socetReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
            new Thread(() -> {
                while (isRun) {
                    try {
                        String line = socetReader.readLine();
                        if (line != null) {
                            Logger.info(">> " + line);
                        }
                    } catch (IOException e) {
                        Logger.error("Server stopped.");
                        System.exit(0);
                    }
                }
            }).start();
            new Thread(() -> {
                BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
                while (isRun) {
                    try {
                        String line = consoleReader.readLine();
                        if (line.equals("disconnect")) {
                            client.close();
                            isRun = false;
                            System.exit(0);
                        }
                        socketWriter.write(line + "\n");
                        socketWriter.flush();
                        Logger.info("<< " + line);
                    } catch (IOException e) {
                        Logger.error(e.getMessage());
                    }
                }
            }).start();
        } catch (IOException ex) {
            Logger.error(ex.getMessage());
        }
    }
}
