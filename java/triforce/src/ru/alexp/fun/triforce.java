package ru.alexp.fun;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Alex_P
 */
class triforce {

    static final String s = "▲";
    static int lines = 2;
    static final boolean postSpaces = true;

    public static void main(String[] args) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Lines: ");
        try {
            lines = Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            System.out.println(e.getMessage());
        }
        System.out.println();

        for (int i = 1; i <= lines; i++) {
            for (int spaces = 0; spaces < lines - i; spaces++) {
                System.out.print(" ");
            }
            for (int treangles = 0; treangles < i; treangles++) {
                System.out.print(s);
            }
            if (postSpaces) {
                for (int spaces = 0; spaces < lines - i; spaces++) {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
