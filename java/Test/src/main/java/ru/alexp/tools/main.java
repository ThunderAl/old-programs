package ru.alexp.tools;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;


/**
 * @author Alex_P
 */
public class main {

    private static Socket client;
    private static Thread clientThread;
    private static BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

    private static ServerSocket server;
    private static Thread serverThread;
    private static int serverPort = -1;
    private static boolean isStarted = false;

    public static void main(String[] args) {
        serverThread = new Thread(() -> {
            runServer(8088);
            do {
                try {
                    isStarted = true;
                    final Socket c = server.accept(); //ловим клиента
                    System.out.println("Client " + c.getInetAddress().toString() + ":" + c.getPort() + " connected.");

                    final BufferedWriter out = new BufferedWriter(new OutputStreamWriter(c.getOutputStream()));

                    out.write("Hello. It's test socket server.\r\n");
                    out.write("Type \"stop\" to stop server.\r\n");
                    out.flush();

                    new Thread(() -> { // просто читалка меседжа
                        try {
                            final BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
                            while (isStarted) {
                                System.out.println("SERVER << " + reader.readLine()); // ждёт пока не поступит строка
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }).start();

                    final BufferedReader in = new BufferedReader(new InputStreamReader(c.getInputStream()));

                    String com;
                    do {
                        com = in.readLine();
                        onServerCommand(com);
                    } while (com.equals("exit"));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } while (isStarted);
            System.out.println("Server stopped.");
            isStarted = false;
        });

        clientThread = new Thread(() -> {
            while (!isStarted) Thread.yield(); // ждём пока сервак запустится

            try {
                client = new Socket("127.0.0.1", serverPort);

                new Thread(() -> { // просто читалка меседжа
                    try {
                        final BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
                        while (isStarted) {
                            System.out.println("<< " + reader.readLine()); // ждёт пока не поступит строка
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }).start();

                final BufferedWriter toserver = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));


                new Thread(() -> {
                    try {
                        while (client.isConnected()) {
                            System.out.print("> ");
                            String comm = console.readLine();
                            if (comm != null) {
                                System.out.println(">> " + comm);
                                toserver.write(comm);
                                toserver.flush();
                            }
                        }
                    } catch (IOException e) {
                    }
                }).start();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        serverThread.start();
        //clientThread.start();
    }

    private static void runServer(int port) {
        try {
            server = new ServerSocket(port);
            serverPort = port;
            System.out.println("Server started on " + port);
        } catch (IOException e) {
            runServer(port + 1);
        }
    }

    private static void onServerCommand(String com) {
        String[] command = com.split(" ");
        switch (command[0]) {
            case "forceStop":
                System.exit(1);
                break;
            case "stop":
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case "echo":
                System.out.println(String.join(" ", Arrays.copyOfRange(command, 1, command.length)));
        }
    }
}
