package ru.alexp.mca.gui;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.alexp.mca.App;
import ru.alexp.mca.ProjectManager;

public class JFXApp extends Application {

    private static JFXApp insance;

    private SceneRegistry registry;
    private Stage stage;
    private ProjectManager projectManager;

    @Override
    public void init() throws Exception {
        super.init();
        insance = this;
    }

    @Override
    public void start(Stage stage) throws Exception {
        GuiApp app = App.get(App.State.GUI);
        this.registry = new SceneRegistry(app.getApp(), this);
        this.stage = stage;
        this.projectManager = new ProjectManager();

        registry.registerDefault();
        projectManager.loadProjects();
        registry.setDefault();
        stage.show();
    }

    public SceneRegistry getRegistry() {
        return registry;
    }

    public Stage getStage() {
        return stage;
    }

    public ProjectManager getProjectManager() {
        return projectManager;
    }

    public static JFXApp getInsance() {
        return insance;
    }
}
