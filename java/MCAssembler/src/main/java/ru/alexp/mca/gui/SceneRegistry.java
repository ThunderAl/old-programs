package ru.alexp.mca.gui;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.alexp.mca.App;
import ru.alexp.mca.gui.scene.MainMenu;
import ru.alexp.mca.gui.scene.ProjectEditor;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class SceneRegistry {

    private final Logger log = Logger.getLogger(SceneRegistry.class.getSimpleName());

    private final Map<String, MCAScene> scenes;
    private final App app;
    private final JFXApp jfxApp;

    public SceneRegistry(App app, JFXApp jfxApp) {
        this.scenes = new HashMap<>();
        this.app = app;
        this.jfxApp = jfxApp;
    }

    public SceneRegistry add(Class<? extends MCAScene> scene) {
        return add(null, scene);
    }

    public SceneRegistry add(String key, Class<? extends MCAScene> scene) {
        final Constructor<? extends MCAScene> c;

        try {
            c = scene.getConstructor(Parent.class, App.class, JFXApp.class);
        } catch (Exception e) {
            e.printStackTrace();
            return this;
        }

        final MCAScene mcascene;

        try {
            mcascene = c.newInstance(new GroupLayoutPane(), app, jfxApp);
        } catch (Exception e) {
            e.printStackTrace();
            return this;
        }

        return key == null ? add(mcascene) : add(key, mcascene);
    }

    public SceneRegistry add(MCAScene scene) {
        return add(scene.getName(), scene);
    }

    public SceneRegistry add(String key, MCAScene scene) {
        scenes.put(key, scene);
        log.fine(String.format("Added new scene %s (%s)", key, scene.getClass().getSimpleName()));
        return this;
    }

    public MCAScene get(String key) {
        return scenes.get(key);
    }

    public MCAScene get(Class<? extends MCAScene> scene) {
        return get(scene.getSimpleName());
    }

    public void registerDefault() {
        add(MainMenu.class);
        add(ProjectEditor.class);
    }

    public void set(String key) {
        final Stage stage = jfxApp.getStage();
        final Scene oldScene = stage.getScene();
        final MCAScene newScene = get(key);
        if (newScene == null) return;
        if (oldScene != null && oldScene instanceof MCAScene)
            ((MCAScene) oldScene).onHide();
        stage.hide();
        newScene.onShow();
        stage.setScene(newScene);
        stage.show();
    }

    public void set(Class<? extends MCAScene> scene) {
        set(scene.getSimpleName());
    }

    public void setDefault(Stage stage) {
        set(MainMenu.class);
    }

    public void setDefault() {
        setDefault(jfxApp.getStage());
    }
}
