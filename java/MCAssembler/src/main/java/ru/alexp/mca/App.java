package ru.alexp.mca;

import ru.alexp.mca.cli.CliApp;
import ru.alexp.mca.gui.GuiApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static ru.alexp.mca.App.State.GUI;

public class App {

    private static final HashMap<State, SidedApp> singleton = new HashMap<>();

    private final State state;
    private final ArrayList<String> cmdArgs;

    private App(State state) {
        this.state = state;
        this.cmdArgs = new ArrayList<>();
    }

    @SuppressWarnings("unchecked")
    public static <T extends SidedApp> T get(State state) {
        return ((T) singleton.get(state));
    }

    public State getState() {
        return state;
    }

    public App addCmdArgs(String[] args) {
        cmdArgs.addAll(Arrays.asList(args));
        return this;
    }

    public ArrayList<String> getCmdArgs() {
        return cmdArgs;
    }

    public void start() {
        switch (getState()) {
            case CONSOLE:
                onConsoleApp();
                break;
            case GUI:
                onGuiApp();
                break;
        }
    }

    private void onGuiApp() {
        GuiApp gui = new GuiApp(this);
        singleton.put(GUI, gui);
        gui.run();
    }

    private void onConsoleApp() {
        CliApp cli = new CliApp(this);
        singleton.put(GUI, cli);
        cli.run();
    }

    public enum State {
        CONSOLE, GUI;

        public App initApp() {
            return new App(this);
        }
    }
}
