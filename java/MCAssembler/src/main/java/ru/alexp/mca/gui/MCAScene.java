package ru.alexp.mca.gui;

import javafx.beans.NamedArg;
import javafx.scene.Parent;
import javafx.scene.Scene;
import ru.alexp.mca.App;

public abstract class MCAScene extends Scene {

    protected final GroupLayoutPane pane;
    private final App app;
    private final JFXApp jfxApp;

    public MCAScene(@NamedArg("root") Parent root, App app, JFXApp jfxApp) {
        super(root);
        this.app = app;
        this.jfxApp = jfxApp;
        if (root instanceof GroupLayoutPane)
            this.pane = ((GroupLayoutPane) root);
        else this.pane = null;
    }

    public App getApp() {
        return app;
    }

    public JFXApp getJFXApp() {
        return jfxApp;
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }

    public void onShow() {
    }

    public void onHide() {
    }
}
