package ru.alexp.mca.gui.common;

import javafx.scene.control.Label;
import javafx.scene.control.OverrunStyle;
import javafx.scene.text.Font;
import ru.alexp.mca.Project;
import ru.alexp.mca.gui.GroupLayoutPane;

public class MCAssemblyListItem extends GroupLayoutPane {

    private final Label name;
    private final Label type;
    private final Label path;
    private final Project prj;

    public MCAssemblyListItem(Project prj) {
        this.prj = prj;
        this.name = new Label(prj.getName());
        this.type = new Label(String.join("\n", prj.getType()));
        this.path = new Label(prj.getPath().getAbsolutePath());

        this.name.setFont(Font.font(20));
        this.type.setStyle("-fx-text-alignment: right");
        this.path.textOverrunProperty().set(OverrunStyle.CENTER_ELLIPSIS);
        this.setMinWidth(220);
        this.setMaxWidth(220);

        getChildren().addAll(this.name, this.type, this.path);

        setVerticalGroup(createSequentialGroup()
                .addGroup(createParallelGroup()
                        .addNode(this.name)
                        .addNode(this.type)
                )
                .addGap(10)
                .addNode(this.path)
        );

        setHorizontalGroup(createParallelGroup()
                .addGroup(createSequentialGroup()
                        .addNode(this.name)
                        .addGlue()
                        .addNode(this.type)
                )
                .addNode(this.path)
        );
    }

    public Project getProject() {
        return prj;
    }
}
