package ru.alexp.mca.gui;

import javafx.application.Application;
import ru.alexp.mca.App;
import ru.alexp.mca.SidedApp;

public class GuiApp extends SidedApp {

    public GuiApp(App app) {
        super(app);
    }

    public void run() {
        Application.launch(JFXApp.class, getApp().getCmdArgs().toArray(new String[0]));
    }
}
