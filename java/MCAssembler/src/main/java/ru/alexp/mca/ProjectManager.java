package ru.alexp.mca;


import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import ru.alexp.mca.gui.JFXApp;
import ru.alexp.mca.gui.scene.ProjectEditor;
import ru.alexp.mca.util.AppData;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class ProjectManager extends ArrayList<Project> {

    private int active = -1;

    public void loadProjects() {
        final File f = AppData.getFile("projects.json");

        JSONArray arr = null;
        if (f.exists()) try {
            arr = new JSONArray(IOUtils.toString(new FileReader(f)));
        } catch (Exception ignored) {
            f.delete();
            return;
        }

        if (arr != null)
            for (int i = 0; arr.length() > i; i++)
                try {
                    add(new Project(arr.getJSONObject(i)));
                } catch (Exception ignored) {
                }
    }

    public void saveProjects() {
        final File f = AppData.getFile("projects.json");
        final JSONArray base = new JSONArray();

        forEach(p -> base.put(p.toJson()));

        try {
            IOUtils.write(base.toString(2), new FileWriter(f));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openProject(Project prj) {
        try {
            selectProject(prj);
            JFXApp.getInsance()
                    .getRegistry()
                    .set(ProjectEditor.class);
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Error while loading project");
            alert.setContentText(e.getLocalizedMessage());

            ButtonType delete = new ButtonType("Remove project");
            ButtonType cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

            alert.getButtonTypes().setAll(delete, cancel);

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == delete) {
                this.remove(prj);
            }

            e.printStackTrace();
        }
    }

    public void selectProject(Project prj) {
        active = indexOf(prj);
    }

    public void selectProject(int index) {
        active = index;
    }

    public Project getActive() {
        return get(active);
    }
}
