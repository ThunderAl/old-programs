package ru.alexp.mca.util.json;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class JsonDataItem<T> {

    private final boolean has;
    private final T data;

    protected JsonDataItem() {
        this(null);
    }

    protected JsonDataItem(T data) {
        this.has = data != null;
        this.data = data;
    }

    public boolean has() {
        return has;
    }

    public JsonDataItem<T> ifHas(Consumer<T> callback) {
        if (has()) callback.accept(data);
        return this;
    }

    public T get() {
        return data;
    }

    public static <T> JsonDataItem<T> undefined() {
        return new JsonDataItem<>();
    }

    public static <T> JsonDataItem<T> from(T data) {
        return new JsonDataItem<>(data);
    }
}
