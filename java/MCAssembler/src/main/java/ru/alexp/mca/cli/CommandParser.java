package ru.alexp.mca.cli;

import org.apache.commons.cli.*;
import ru.alexp.mca.App;

import java.util.ArrayList;

public class CommandParser {

    private final String[] args;
    private final Options options;

    public CommandParser(ArrayList<String> args) {
        this.options = new Options();
        this.args = args.toArray(new String[args.size()]);
    }

    public void printHelp() {
        HelpFormatter help = new HelpFormatter();
        createOptions();
        help.printHelp("mca", "Minecraft assembler by Alex_P\n ", options, "");
    }

    public void createOptions() {
        if (options.getOptions().size() > 0) return;

        options.addOption(Option.builder()
                .longOpt("gui")
                .desc("Interrupts all cli action and starts gui interface.")
                .build());

        options.addOption(Option.builder("h")
                .longOpt("help")
                .desc("Prints this help.")
                .build());
    }

    public void execute() {
        final DefaultParser parser = new DefaultParser();
        final CommandLine cl;
        try {
            createOptions();
            cl = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            System.out.println();
            printHelp();
            return;
        }

        if (cl.hasOption("gui")) {
            System.out.println("Starting gui...");
            App.State.GUI.initApp().start();
            return;
        }

        if (cl.hasOption('h')) {
            printHelp();
            return;
        }
    }
}
