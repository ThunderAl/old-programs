package ru.alexp.mca;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Project {

    private final HashMap<String, Object> data;
    private final String name;
    private final ArrayList<String> type;
    private final File path;

    public Project(String name, ArrayList<String> type, File path, HashMap<String, Object> data) {
        this.data = data;
        this.name = name;
        this.type = type;
        this.path = path;
    }

    public Project(JSONObject json) {
        this.name = json.getString("name");

        this.data = new HashMap<>();
        if (json.has("data")) {
            JSONArray jsonData = json.optJSONArray("data");
            for (int i = 0; i > jsonData.length(); i++) {
                JSONObject row = jsonData.optJSONObject(i);
                if (!(row.has("key") && row.has("data"))) continue;
                try {
                    String data = row.getString("data");
                    if (data.startsWith("{"))
                        this.data.put(row.getString("key"), new JSONObject(data));
                    else if (data.startsWith("["))
                        this.data.put(row.getString("key"), new JSONArray(data));
                    else throw new JSONException("");
                } catch (JSONException ignored) {
                    try {
                        byte[] rawData = Base64.decodeBase64(row.getString("data"));
                        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(rawData));
                        this.data.put(row.getString("key"), ois.readObject());
                        ois.close();
                    } catch (Exception ignored0) {
                        this.data.put(row.getString("key"), row.getString("data"));
                    }
                }
            }
        }

        this.type = new ArrayList<>();
        if (json.has("type")) {
            JSONArray jsonTypes = json.getJSONArray("type");
            for (int i = 0; i < jsonTypes.length(); i++)
                this.type.add(jsonTypes.optString(i));
        }
        
        this.path = new File(json.getString("path"));
    }

    public String getName() {
        return name;
    }

    public ArrayList<String> getType() {
        return type;
    }

    public File getPath() {
        return path;
    }

    public boolean hasData(String key) {
        return data.containsKey(key);
    }

    @SuppressWarnings("unchecked")
    public <T> T getData(String key, T def) {
        return hasData(key) ? (T) data.get(key) : def;
    }

    public HashMap<String, Object> getData() {
        return data;
    }

    public JSONObject toJson() {
        JSONObject project = new JSONObject();
        project.put("name", this.name);
        project.put("path", this.path.getAbsolutePath());
        project.put("type", new JSONArray(this.type));

        JSONArray data = new JSONArray();
        getData().forEach((s, o) -> {
            JSONObject node = new JSONObject();
            node.put("key", s);
            try {
                if (o instanceof Serializable) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(o);
                    oos.close();
                    node.put("data", baos.toString());
                } else throw new Exception();
            } catch (Exception ignored) {
                node.put("data", o);
            }
            data.put(node);
        });

        project.put("data", data);

        return project;
    }
}
