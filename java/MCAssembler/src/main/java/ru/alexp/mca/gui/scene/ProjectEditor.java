package ru.alexp.mca.gui.scene;

import javafx.beans.NamedArg;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import ru.alexp.mca.App;
import ru.alexp.mca.gui.JFXApp;
import ru.alexp.mca.gui.MCAScene;

public class ProjectEditor extends MCAScene {

    private final Label title;

    public ProjectEditor(@NamedArg("root") Parent root, App app, JFXApp jfxApp) {
        super(root, app, jfxApp);

        title = new Label();

        pane.getChildren().setAll(title);

        pane.setHorizontalGroup(pane.createSequentialGroup()
                .addNode(title)
                .addGlue()
        );
        pane.setVerticalGroup(pane.createSequentialGroup()
                .addNode(title)
        );
    }

    @Override
    public void onShow() {
        title.setText(getJFXApp().getProjectManager().getActive().getName());
        System.out.println(getJFXApp().getProjectManager().getActive().getName());
    }
}
