package ru.alexp.mca.util.json;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonObject {

    private final JSONObject target;

    public JsonObject(JSONObject o) {
        target = o;
    }

    public JsonObject() {
        this(new JSONObject());
    }

    public JSONObject asOld() {
        return target;
    }

    public boolean has(String key) {
        return asOld().has(key);
    }

    public JsonDataItem<Integer> asInt(String key) {
        try {
            return JsonDataItem.from(asOld().getInt(key));
        } catch (JSONException ignored) {
            return JsonDataItem.undefined();
        }
    }

    public JsonDataItem<String> asString(String key) {
        try {
            return JsonDataItem.from(asOld().getString(key));
        } catch (JSONException ignored) {
            return JsonDataItem.undefined();
        }
    }

    public JsonDataItem<Boolean> asBool(String key) {
        try {
            return JsonDataItem.from(asOld().getBoolean(key));
        } catch (JSONException ignored) {
            return JsonDataItem.undefined();
        }
    }

    public JsonDataItem<JSONArray> asJsonArray(String key) {
        try {
            return JsonDataItem.from(asOld().getJSONArray(key));
        } catch (JSONException ignored) {
            return JsonDataItem.undefined();
        }
    }

    public static JsonObject create() {
        return new JsonObject();
    }

    public static JsonObject from(JSONObject o) {
        return new JsonObject(o);
    }

    public static JsonObject from(String json) {
        return from(new JSONObject(json));
    }
}
