package ru.alexp.mca;

public class MCAssembler {

    public static void main(String[] args) {

        final App app;

        if (args.length > 0)
            app = App.State.CONSOLE.initApp().addCmdArgs(args);
        else
            app = App.State.GUI.initApp();

        app.start();
    }
}
