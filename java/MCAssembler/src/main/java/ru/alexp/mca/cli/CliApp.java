package ru.alexp.mca.cli;

import ru.alexp.mca.App;
import ru.alexp.mca.SidedApp;

public class CliApp extends SidedApp {

    private final CommandParser cli;

    public CliApp(App app) {
        super(app);
        cli = new CommandParser(app.getCmdArgs());
    }

    public void run() {
        cli.execute();
    }
}
