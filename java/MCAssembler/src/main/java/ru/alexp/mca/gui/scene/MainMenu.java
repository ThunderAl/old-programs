package ru.alexp.mca.gui.scene;

import javafx.beans.NamedArg;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import ru.alexp.mca.App;
import ru.alexp.mca.gui.JFXApp;
import ru.alexp.mca.gui.MCAScene;
import ru.alexp.mca.gui.common.MCAssemblyListItem;

public class MainMenu extends MCAScene {

    private final ListView<MCAssemblyListItem> projects;
    private final Button create;
    private final Button open;

    public MainMenu(@NamedArg("root") Parent root, App app, JFXApp jfxApp) {
        super(root, app, jfxApp);

        projects = new ListView<>();
        create = new Button("Create new project");
        open = new Button("Open existing project");

        projects.setOnMouseClicked(event -> {
            if (event.getClickCount() >= 2)
                jfxApp.getProjectManager().openProject(projects.getSelectionModel().getSelectedItem().getProject());
        });

        pane.getChildren().addAll(projects, create, open);

        pane.setHorizontalGroup(pane.createSequentialGroup()
                .addNode(projects, 250)
                .addGap(50, 150, Short.MAX_VALUE)
                .addGroup(pane.createParallelGroup()
                        .addNode(create, 200)
                        .addNode(open, 200)
                )
                .addGap(50, 150, Short.MAX_VALUE)
        );
        pane.setVerticalGroup(pane.createParallelGroup()
                .addNode(projects)
                .addGroup(pane.createSequentialGroup()
                        .addGlue()
                        .addNode(create)
                        .addGap(5)
                        .addNode(open)
                        .addGlue()
                        .addGlue()
                )
        );
    }

    @Override
    public void onShow() {
        projects.getItems().clear();

        getJFXApp().getProjectManager().forEach(p ->
                projects.getItems().add(new MCAssemblyListItem(p))
        );
    }
}
