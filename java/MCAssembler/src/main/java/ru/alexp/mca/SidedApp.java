package ru.alexp.mca;

public abstract class SidedApp {
    protected final App app;

    public SidedApp(App app) {
        this.app = app;
    }

    public App getApp() {
        return app;
    }

    public abstract void run();
}
